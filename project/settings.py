# Get the project directory
import os
BASE_DIR = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))

# Configurations
DEBUG               = True
AUTH_USER_MODEL     = 'users.User'
SECRET_KEY          = '+&cs@%y5-z4#5rw@kayk8%_4k5#ur3om0tu-4dl99xw2p*9+9k'
ROOT_URLCONF        = 'project.urls'
WSGI_APPLICATION    = 'project.wsgi.application'
ASGI_APPLICATION    = 'project.routing.application'
LANGUAGE_CODE       = 'en-us'
TIME_ZONE           = 'UTC'
USE_I18N            = True
USE_L10N            = True
USE_TZ              = True
STATIC_URL          = '/static/'
STATIC_ROOT         = os.path.join(BASE_DIR, 'staticfiles')

MEDIA_URL           = '/media/'
MEDIA_ROOT          = os.path.join(BASE_DIR, 'media')
# Allow third party hosts here
ALLOWED_HOSTS = ['*']

# Install your apps here
INSTALLED_APPS = [
    # Project generic installs
    'rcrud',
    'users',
    'mainApp',
    'notifications.apps.NotificationsConfig',

    # Third party installs
    'rest_framework',
    'rest_framework.authtoken',
    'channels',

    # Django generic installs
    'django.contrib.admin',
    'django.contrib.auth',
    'django.contrib.contenttypes',
    'django.contrib.sessions',
    'django.contrib.messages',
    'django.contrib.staticfiles',
]

# Rest Framework Configuration
REST_FRAMEWORK = {
    'DEFAULT_AUTHENTICATION_CLASSES': [
        'rest_framework.authentication.TokenAuthentication'
    ],
    'DEFAULT_PERMISSION_CLASSES': [
        'rest_framework.permissions.IsAuthenticated'
    ]
}


CHANNEL_LAYERS = {
    'default': {
        'BACKEND': 'channels_redis.core.RedisChannelLayer',
        'CONFIG': {
            "hosts": [('127.0.0.1', 6379)],
        },
    },
}


# Put your middlewares here
MIDDLEWARE = [
    'django.middleware.security.SecurityMiddleware',
    'django.contrib.sessions.middleware.SessionMiddleware',
    'django.middleware.common.CommonMiddleware',
    'django.middleware.csrf.CsrfViewMiddleware',
    'django.contrib.auth.middleware.AuthenticationMiddleware',
    'django.contrib.messages.middleware.MessageMiddleware',
    'django.middleware.clickjacking.XFrameOptionsMiddleware',
]



# Database configurations
DATABASES = {
    'default': {
        'ENGINE':   'django.db.backends.sqlite3',
        'NAME':     os.path.join(BASE_DIR, 'db.sqlite3'),
    }
}

# Templates configurations
TEMPLATES = [
    {
        'BACKEND':  'django.template.backends.django.DjangoTemplates',
        'DIRS':     [os.path.join(BASE_DIR, 'templates')],
        'APP_DIRS': True,
        'OPTIONS': {
            'context_processors': [
                'django.template.context_processors.debug',
                'django.template.context_processors.request',
                'django.contrib.auth.context_processors.auth',
                'django.contrib.messages.context_processors.messages',
            ],
        },
    },
]

# Password validators
AUTH_PASSWORD_VALIDATORS = [
    {
        'NAME': 'django.contrib.auth.password_validation.UserAttributeSimilarityValidator',
    },
    {
        'NAME': 'django.contrib.auth.password_validation.MinimumLengthValidator',
    },
    {
        'NAME': 'django.contrib.auth.password_validation.CommonPasswordValidator',
    },
    {
        'NAME': 'django.contrib.auth.password_validation.NumericPasswordValidator',
    },
]


