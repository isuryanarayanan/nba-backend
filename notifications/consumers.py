from channels.generic.websocket import AsyncWebsocketConsumer
from rest_framework.authtoken.models import Token
from django.db.models.signals import post_save
from channels.layers import get_channel_layer
from asgiref.sync import async_to_sync
from .models import NotificationModel
import json
from users.profiles import teacherProfile

def getDepartment(user):
    tProfile = teacherProfile.objects.filter(user=user)
    return tProfile.first().department.user

def create_notification(user, title, message, viewed, **kwargs):
    NotificationModel.objects.create(title=title, message=message, viewed=viewed)
    if(user):
        if user.is_teacher:
            dep = getDepartment(user)
            channel_layer = get_channel_layer()
            async_to_sync(channel_layer.group_send)(
                dep.group_name,
                {
                    'type': 'channel_message',
                    'message': message,
                    'owner': user.email
                }
            )
            # Send notification to department        
        channel_layer = get_channel_layer()
        async_to_sync(channel_layer.group_send)(
            user.group_name,
            {
                'type': 'channel_message',
                'message': message,
                'owner': user.email
            }
        )

class NotificationConsumer(AsyncWebsocketConsumer):

    async def connect(self):

        try:
            token = self.scope.get('url_route', {}).get('kwargs', {}).get('token', False)

            if not token:
                await self.close()

            try:
                token = Token.objects.select_related('user').get(key=token)

            except Token.DoesNotExist:
                await self.close()

            if not token.user.is_active:
                await self.close()

            user = token.user

            group_name = user.group_name

            await self.channel_layer.group_add(
                group_name,
                self.channel_name,
            )

            await self.accept()

        except Exception as e:
            await self.close()

    async def channel_message(self, event):
        message = event['message']
        owner = event['owner']


        await self.send(text_data=json.dumps({
            'message': message,
            'owner': owner
        }))


    async def disconnect(self, code):
        try:
            # Get auth token from url.
            token = self.scope.get('url_route', {}).get(
                'kwargs', {}).get('token', False)
            try:
                token = Token.objects.select_related('user').get(key=token)
            except Token.DoesNotExist:
                pass
            user = token.user

            # Get the group from which user is to be kicked.
            group_name = user.group_name

            # kick this channel from the group.
            await self.channel_layer.group_discard(group_name, self.channel_name)
        except Exception as e:
            print(e)