from django.conf import settings
from django.dispatch import receiver
from django.db.models.signals import post_save
from notifications.models import NotificationModel, ActivityTrack
def getTrack(classmodel):
    try:
        return classmodel.objects.all().first()
    except classmodel.DoesNotExist:
        return None

@receiver(post_save, sender=NotificationModel)
def add_to_activities(sender, instance=None, created=False, **kwargs):
    if created:
        act = getTrack(ActivityTrack)
        if act == None:
            print("create activity track model")
            act = ActivityTrack.objects.create()
            print("act id:"+str(act.id))
            act.activities.add(instance)

        else:
            act.activities.add(instance)
            print("add notification to track")
            print(act)

