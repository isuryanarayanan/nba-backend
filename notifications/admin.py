from django.contrib import admin

# Register your models here.
from notifications.models import NotificationModel, ActivityTrack
admin.site.register(NotificationModel)
admin.site.register(ActivityTrack)