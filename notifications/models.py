from django.db import models

# Create your models here.
class NotificationModel(models.Model):
    
    title = models.CharField(max_length=256)
    message = models.TextField()
    timestamp = models.DateTimeField(auto_now=True)
    viewed = models.BooleanField(default=False)

    class Meta:
        app_label = 'notifications'
    
    def __str__(self):
        return f'{self.title}'

class ActivityTrack(models.Model):
    
    activities = models.ManyToManyField(NotificationModel)

    class Meta:
        app_label = 'notifications'