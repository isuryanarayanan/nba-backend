# Generated by Django 3.0.3 on 2020-02-22 09:07

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('users', '0008_remove_auditorprofile_revokeaccess'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='departmentprofile',
            name='revokeAccess',
        ),
        migrations.RemoveField(
            model_name='teacherprofile',
            name='revokeAccess',
        ),
    ]
