from django.db      import models
from users.models   import User

class auditorProfile(models.Model):
    user = models.OneToOneField(User, on_delete=models.CASCADE)

    class Meta:
        app_label = 'users'
    
    def __str__(self):
        return f'{self.user.email} | Auditor profile'
        
class institutionProfile(models.Model):
    user = models.OneToOneField(User, on_delete=models.CASCADE)

    class Meta:
        app_label = 'users'
    
    def __str__(self):
        return f'{self.user.email} | Institution profile'

class departmentProfile(models.Model):
    user = models.OneToOneField(User, on_delete=models.CASCADE)
    institution = models.OneToOneField(institutionProfile, on_delete=models.CASCADE, null=True)

    class Meta:
        app_label = 'users'
    
    def __str__(self):
        return f'{self.user.email} | Department profile'

class teacherProfile(models.Model):
    user = models.OneToOneField(User, on_delete=models.CASCADE)
    department = models.OneToOneField(departmentProfile, on_delete=models.CASCADE, null=True)
    hasPerm = models.BooleanField(default=False)

    class Meta:
        app_label = 'users'
    
    def __str__(self):
        return f'{self.user.email} | Teacher profile'
