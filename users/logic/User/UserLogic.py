from rest_framework.serializers import ModelSerializer
from django.views.decorators.csrf import csrf_exempt
from rest_framework.decorators import api_view
from rest_framework.response import Response
from django.http import JsonResponse
from django.contrib import admin
from users.passwords import passwordField
from rest_framework import serializers

def getSingleProfile(args):
    result = False
    for a in args:
        if a:
            if result:
                return False
            else:
                result = True
    return result

@api_view(['GET', ])
def _detailGet(request, id, _model):

    class _serializer(ModelSerializer):
        class Meta:
            model = _model
            fields = '__all__'

    try:
        objects = _model.objects.get(id=id)
    except _model.DoesNotExist:
        return Response({'object':'not found'},status=404)

    if request.method == 'GET':
        serializer = _serializer(objects)
        return Response(serializer.data)

@api_view(['PUT', ])
def _put(request, id, _model):
    obj = _model.objects.get(id = id)
    requser = request.user
    class RegistrationSerializer(ModelSerializer):
        class Meta:
            model = _model
            fields = ['email', 'teacher', 'department', 'institution', 'auditor', 'revokeAccess']


        def save(self):
            user = obj
            email =  self.validated_data['email']
            teacher = self.validated_data['teacher']
            department = self.validated_data['department']
            institution = self.validated_data['institution']
            auditor = self.validated_data['auditor']
            revokeAccess = self.validated_data['revokeAccess']

            args = [teacher,department,institution,auditor]

            if getSingleProfile(args) == False:
                raise serializers.ValidationError({'profile': 'atleast one and only one profile is allowed'})
            if requser.is_auditor:
                raise serializers.ValidationError({'profile': 'denied permission'})
                
            if requser.is_department and (institution or auditor):
                raise serializers.ValidationError({'profile': 'denied permission'})

            if requser.is_teacher:
                raise serializers.ValidationError({'profile': 'denied permission'})

            if revokeAccess != user.revokeAccess and requser.is_teacher:
                raise serializers.ValidationError({'profile': 'denied permission'})

            if revokeAccess != user.revokeAccess and requser.is_auditor:
                raise serializers.ValidationError({'profile': 'denied permission'})

            if revokeAccess != user.revokeAccess and requser.is_department and (user.is_institution or user.is_auditor):
                raise serializers.ValidationError({'profile': 'denied permission'})
                

            user.email = email
            user.teacher = teacher
            user.department = department
            user.institution = institution
            user.auditor = auditor
            user.revokeAccess = revokeAccess
            user.save()
            return user
    serializer = RegistrationSerializer(obj, data=request.data, partial=True)
    data = {}
    if serializer.is_valid():
        user = serializer.save()
        data['response']        = 'successfully registered'
        data['email']           = user.email
        data['is_teacher']      = user.is_teacher
        data['is_department']   = user.is_department
        data['is_institution']  = user.is_institution
        data['is_auditor']      = user.is_auditor
        data['revokeAccess']    = user.revokeAccess

    else:
        data = serializer.errors
    return  Response(data)

@api_view(['DELETE', ])
def _delete(request, id, _model):

    try:
        objects = _model.objects.get(id=id)
    except _model.DoesNotExist:
        return Response({'object':'not found'},status=404)

    if request.method == 'DELETE':
        if(objects):
            if(request.user == objects):
                objects.delete()
                return Response({'success':'instance deleted'}, status = 200)
            return Response({'denied':'instance cannot be deleted'}, status = 200)
        else:
            return Response('Not Found', status = 404)
    else:
        return Response('Not Found', status = 404)

@api_view(['GET', ])
def _get(request, _model):

    class _serializer(ModelSerializer):
        class Meta:
            model = _model
            fields = '__all__'

    if request.method == 'GET':
        serializer = _serializer(_model.objects.all(), many=True)
        return Response(serializer.data)

@api_view(['POST', ])
def _post(request, _model):
    
    class RegistrationSerializer(ModelSerializer):
        # password2 = serializers.CharField(style={'input_type':'password'},write_only=True)

        class Meta:
            model = _model
            fields = ['email', 'teacher', 'department', 'institution', 'auditor','revokeAccess']


        def save(self):
            user = _model(
                email = self.validated_data['email']
            )
            password = passwordField
            password2 = passwordField

        
            teacher = self.validated_data['teacher']
            department = self.validated_data['department']
            institution = self.validated_data['institution']
            auditor = self.validated_data['auditor']

            args = [teacher,department,institution,auditor]
            requser = request.user
            if getSingleProfile(args) == False:
                raise serializers.ValidationError({'profile': 'atleast one and only one profile is allowed'})
            print(requser.is_teacher)
            if requser.is_auditor:
                raise serializers.ValidationError({'profile': 'denied permission'})
                
            if requser.is_department and (institution or auditor):
                raise serializers.ValidationError({'profile': 'denied permission'})

            if requser.is_teacher:
                raise serializers.ValidationError({'profile': 'denied permission'})


            if password != password2:
                raise serializers.ValidationError({'password':'Passwords must match!'})
            user.teacher = teacher
            user.department = department
            user.institution = institution
            user.auditor = auditor
            user.revokeAccess = False
            user.set_password(password)
            user.save()
            return user

    serializer = RegistrationSerializer(data=request.data)
    
    data = {}
    if serializer.is_valid():

        user = serializer.save()
        data['response']        = 'successfully registered'
        data['email']           = user.email
        data['is_teacher']      = user.is_teacher
        data['is_department']   = user.is_department
        data['is_institution']  = user.is_institution
        data['is_auditor']      = user.is_auditor
        data['revokeAccess']    = user.revokeAccess
    else:
        data = serializer.errors
    return  Response(data)

@csrf_exempt
def CRUDView(request,id=None,**kwargs):
    _model = kwargs['model']
    processed = False
    if id ==  None:
        if request.method == 'GET':
            processed = True
            return _get(request, _model)
        if request.method == 'POST':
            processed = True
            return _post(request, _model)
    else:
        if request.method == 'GET':
            processed = True
            return _detailGet(request, id, _model)
        if request.method == 'PUT':
            processed = True
            return _put(request, id, _model)
        if request.method == 'DELETE':
            processed = True
            return _delete(request, id, _model)

    if(processed==False):
        return JsonResponse({'request':'unsupported type'})