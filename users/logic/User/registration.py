from rest_framework import serializers
from users.models import User
from users.passwords import passwordField
#your api serializers here
def getSingleProfile(args):
    result = False
    for a in args:
        if a:
            if result:
                return False
            else:
                result = True
    return result

class RegistrationSerializer(serializers.ModelSerializer):
    # password2 = serializers.CharField(style={'input_type':'password'},write_only=True)

    class Meta:
        model = User
        fields = ['email', 'teacher', 'department', 'institution', 'auditor']


    def save(self):
        user = User(
            email = self.validated_data['email']
        )
        password = passwordField
        password2 = passwordField

    
        teacher = self.validated_data['teacher']
        department = self.validated_data['department']
        institution = self.validated_data['institution']
        auditor = self.validated_data['auditor']

        args = [teacher,department,institution,auditor]

        if getSingleProfile(args) == False:
            raise serializers.ValidationError({'profile': 'atleast one and only one profile is allowed'})

        if password != password2:
            raise serializers.ValidationError({'password':'Passwords must match!'})
        user.teacher = teacher
        user.department = department
        user.institution = institution
        user.auditor = auditor
        user.set_password(password)
        user.save()

        
        
        return user

class AlphaRegistrationSerializer(serializers.ModelSerializer):
    password2 = serializers.CharField(style={'input_type':'password'},write_only=True)

    class Meta:
        model = User
        fields = ['email','password', 'password2', 'teacher', 'department', 'institution', 'auditor']


    def save(self):
        user = User(
            email = self.validated_data['email']
        )
        password = self.validated_data['password']
        password2 = self.validated_data['password2']

    
        teacher = self.validated_data['teacher']
        department = self.validated_data['department']
        institution = self.validated_data['institution']
        auditor = self.validated_data['auditor']

        args = [teacher,department,institution,auditor]

        if getSingleProfile(args) == False:
            raise serializers.ValidationError({'profile': 'atleast one and only one profile is allowed'})

        if password != password2:
            raise serializers.ValidationError({'password':'Passwords must match!'})
        user.teacher = teacher
        user.department = department
        user.institution = institution
        user.auditor = auditor
        user.set_password(password)
        user.save()

        
        
        return user