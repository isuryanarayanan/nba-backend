from rest_framework.serializers import ModelSerializer
from django.views.decorators.csrf import csrf_exempt
from rest_framework.decorators import api_view
from rest_framework.response import Response
from django.http import JsonResponse
from django.contrib import admin


@api_view(['GET', ])
def _detailGet(request, id, _model):

    class _serializer(ModelSerializer):
        class Meta:
            model = _model
            fields = '__all__'

    try:
        objects = _model.objects.get(id=id)
    except _model.DoesNotExist:
        return Response({'object':'not found'},status=404)

    if request.method == 'GET':
        serializer = _serializer(objects)
        return Response(serializer.data)

@api_view(['PUT', ])
def _put(request, id, _model):
    requser = request.user
    obj = _model.objects.get(id=id)
    class _serializer(ModelSerializer):
        class Meta:
            model = _model
            fields = '__all__'

    try:
        objects = _model.objects.get(id=id)
    except _model.DoesNotExist:
        return Response({'object':'not found'},status=404)

    if request.method == 'PUT':
        serializer = _serializer(objects,data=request.data, partial=True)
    if serializer.is_valid():
        serializer.save()

        if requser.is_teacher:
            raise serializers.ValidationError({'profile': 'denied permission'})

        if requser.is_auditor:
            raise serializers.ValidationError({'profile': 'denied permission'})

        return Response(serializer.data)
    else:
        return Response(serializer.errors)




@api_view(['GET', ])
def _get(request, _model):

    class _serializer(ModelSerializer):
        class Meta:
            model = _model
            fields = '__all__'

    if request.method == 'GET':
        serializer = _serializer(_model.objects.all(), many=True)
        return Response(serializer.data)




@csrf_exempt
def CRUDView(request,id=None,**kwargs):
    _model = kwargs['model']
    processed = False
    if id ==  None:
        if request.method == 'GET':
            processed = True
            return _get(request, _model)

    else:
        if request.method == 'GET':
            processed = True
            return _detailGet(request, id, _model)
        if request.method == 'PUT':
            processed = True
            return _put(request, id, _model)

    if(processed==False):
        return JsonResponse({'request':'unsupported type'})