from django.contrib.auth.admin  import UserAdmin as BaseUserAdmin
from django.contrib             import admin
from rcrud.bind                 import Publish
from .models                    import User
from .urls                      import urlpatterns as url
from django.contrib.auth.forms  import UserCreationForm, UserChangeForm
from .profiles                  import (
    auditorProfile,
    institutionProfile,
    departmentProfile,
    teacherProfile
)
# Register your models here.

class UserAdmin(BaseUserAdmin):

    form = UserChangeForm
    add_form = UserCreationForm

    list_display = ('email', 'admin')
    list_filter = ('admin',)
    fieldsets = (
        (None, {'fields': ('email', 'password', 'institution', 'department', 'teacher', 'auditor', 'revokeAccess')}),
        ('Personal info', {'fields': ()}),
        ('Permissions', {'fields': ('admin',)}),
    )

    add_fieldsets = (
        (None, {
            'classes': ('wide',),
            'fields': ('email', 'password1', 'password2', 'institution', 'department', 'teacher', 'auditor', 'revokeAccess')}
        ),
    )
    search_fields = ('email',)
    ordering = ('email',)
    filter_horizontal = ()


admin.site.register(User, UserAdmin)
# Bindin all the models to the rcrud api

url += Publish(User, 'user/', _build=True)

admin.site.register(auditorProfile)
# url += Publish(auditorProfile, 'user/auditor/', _build=True)

admin.site.register(institutionProfile)
# url += Publish(institutionProfile, 'user/institution/', _build=True)

admin.site.register(departmentProfile)
# url += Publish(departmentProfile, 'user/department/', _build=True)

admin.site.register(teacherProfile)
url += Publish(teacherProfile, 'user/teacher/perm/', _build=True)

