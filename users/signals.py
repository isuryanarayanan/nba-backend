from django.dispatch import receiver
from django.db.models.signals import post_save

from rest_framework.authtoken.models import Token
from users.models import User

from users.profiles import teacherProfile
from users.profiles import departmentProfile
from users.profiles import institutionProfile
from users.profiles import auditorProfile

@receiver(post_save, sender=User)
def create_auth_token(sender, instance=None, created=False, **kwargs):
    if created:
        Token.objects.create(user=instance)


@receiver(post_save, sender=User)
def createUserProfile(sender, instance=None, created=False, **kwargs):
    if created:
        if instance.is_teacher:
            student = teacherProfile.objects.create(user=instance)
        if instance.is_department:
            student = departmentProfile.objects.create(user=instance)
        if instance.is_institution:
            student = institutionProfile.objects.create(user=instance)
        if instance.is_auditor:
            student = auditorProfile.objects.create(user=instance)
