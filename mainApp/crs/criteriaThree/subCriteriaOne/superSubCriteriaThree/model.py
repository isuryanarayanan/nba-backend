from django.db import models
from mainApp.crs.criteriaThree.subCriteriaOne.model import c_3_1
from mainApp.crs.mediaStorage.model import mediaStorage
class c_3_1_3(models.Model):
    
    subcriteria = models.ForeignKey(c_3_1, on_delete=models.CASCADE)
    name = models.CharField(default="A, B Program level Course - PO - PSO matrix of all courses INCLUDING first year courses", max_length=250)
    media = models.ManyToManyField(mediaStorage, blank=True)

    class Meta:
        app_label = "mainApp"
