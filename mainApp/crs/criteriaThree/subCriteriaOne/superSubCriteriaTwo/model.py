from django.db import models
from mainApp.crs.criteriaThree.subCriteriaOne.model import c_3_1
from mainApp.crs.mediaStorage.model import mediaStorage
class c_3_1_2(models.Model):
    
    subcriteria = models.ForeignKey(c_3_1, on_delete=models.CASCADE)
    name = models.CharField(default="CO-PO matrices of courses selected in 3.1.1(Six matrices to be mentioned;one per semester from 3rd to 8th semester)", max_length=250)
    poAttainment = models.ManyToManyField(mediaStorage)    

    class Meta:
        app_label = "mainApp"
