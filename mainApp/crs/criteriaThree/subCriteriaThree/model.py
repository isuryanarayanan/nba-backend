from django.db import models
from mainApp.crs.criteriaThree.model import c_3
#from mainApp.crs.criteriaThree.mediaStorage.model import mediaStorage

class c_3_3(models.Model):

    criteria = models.ForeignKey(c_3, on_delete=models.CASCADE)
    name = models.CharField(default="Attainment of Program Outcomes and Program Specific Outcomes", max_length=300)
    #media = models.ManyToManyField(mediaStorage, blank=True)

    class Meta:
        app_label = "mainApp"