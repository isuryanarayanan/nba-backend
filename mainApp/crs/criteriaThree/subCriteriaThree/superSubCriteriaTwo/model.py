from django.db import models
from mainApp.crs.criteriaThree.subCriteriaThree.model import c_3_3
from mainApp.crs.mediaStorage.model import mediaStorage
class c_3_3_2(models.Model):
    
    subcriteria = models.ForeignKey(c_3_3, on_delete=models.CASCADE)
    name = models.CharField(default="Provide results of evaluation of each PO & PSO", max_length=250)
    media = models.ManyToManyField(mediaStorage)


    class Meta:
        app_label = "mainApp"
