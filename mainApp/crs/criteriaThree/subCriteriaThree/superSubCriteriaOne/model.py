from django.db import models
from mainApp.crs.criteriaThree.subCriteriaThree.model import c_3_3
from mainApp.crs.mediaStorage.model import mediaStorage
class c_3_3_1(models.Model):
    
    subcriteria = models.ForeignKey(c_3_3, on_delete=models.CASCADE)
    name = models.CharField(default="Describe assessment tools and processes used for measuring the attainment of each Program Outcome and Program Specific Outcomes", max_length=250)
    description = models.FileField(upload_to="cr3/sub3/sub1/", max_length=20)
    media = models.ManyToManyField(mediaStorage)


    class Meta:
        app_label = "mainApp"
