from django.db import models
from mainApp.crs.criteriaThree.subCriteriaTwo.model import c_3_2
from mainApp.crs.mediaStorage.model import mediaStorage
class c_3_2_1(models.Model):
    
    subcriteria = models.ForeignKey(c_3_2, on_delete=models.CASCADE)
    name = models.CharField(default="Describe the assessment tools and processes used to gather the data upon which the evaluation of Course Outcome is based", max_length=250)
    description = models.FileField(upload_to="cr3/sub2/sub1/", max_length=20)
    media = models.ManyToManyField(mediaStorage)


    class Meta:
        app_label = "mainApp"
