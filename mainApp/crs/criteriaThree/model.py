from django.db import models

class c_3(models.Model):

    name = models.CharField(default="Vision, Mission and Program Educational Objectives", max_length=250)

    class Meta:
        app_label = "mainApp"