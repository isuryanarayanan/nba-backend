from django.db import models
from mainApp.crs.criteriaFour.model import c_4

class c_4_1(models.Model):

    name = models.CharField(default="Enrolment Ratio" ,max_length=250)
    criteria = models.ForeignKey(c_4, on_delete=models.CASCADE)
    year_1 = models.CharField(max_length=250)
    year_2 = models.CharField(max_length=250)
    year_3 = models.CharField(max_length=250)

    n_1 = models.CharField(max_length=250)
    n_2 = models.CharField(max_length=250)
    n_3 = models.CharField(max_length=250)

    n1_1 = models.CharField(max_length=250)
    n1_2 = models.CharField(max_length=250)
    n1_3 = models.CharField(max_length=250)

    er_1 = models.CharField(max_length=250)
    er_2 = models.CharField(max_length=250)
    er_3 = models.CharField(max_length=250)

    class Meta:
        app_label = "mainApp"