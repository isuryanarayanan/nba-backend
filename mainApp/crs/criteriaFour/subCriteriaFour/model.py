from django.db import models
from mainApp.crs.criteriaFour.model import c_4

class c_4_4(models.Model):

    name = models.CharField(default=" Academic Performance in Second Year " ,max_length=250)
    subcriteria = models.ForeignKey(c_4, on_delete=models.CASCADE)
    item_1 = models.CharField(max_length=250)
    item_2 = models.CharField(max_length=250)
    item_3 = models.CharField(max_length=250)
    item_4 = models.CharField(max_length=250)
    

    caym0_1 = models.CharField(max_length=250)
    caym0_2 = models.CharField(max_length=250)
    caym0_3 = models.CharField(max_length=250)
    caym0_4 = models.CharField(max_length=250)

    caym1_1 = models.CharField(max_length=250)
    caym1_2 = models.CharField(max_length=250)
    caym1_3 = models.CharField(max_length=250)
    caym1_4 = models.CharField(max_length=250)

    caym2_1 = models.CharField(max_length=250)
    caym2_2 = models.CharField(max_length=250)
    caym2_3 = models.CharField(max_length=250)
    caym2_4 = models.CharField(max_length=250)

    class Meta:
        app_label = "mainApp"