from django.db import models
from mainApp.crs.criteriaFour.subCriteriaTwo.model import c_4_2

class c_4_2_2(models.Model):
    name = models.CharField(default="", max_length=250)
    criteria = models.ForeignKey(c_4_2, on_delete=models.CASCADE)
    
    item_1 = models.CharField(default="Number of students admitted in the corresponding First Year + admitted in 2nd year via lateral entry and separate division, if applicable", max_length=300)
    item_2 = models.CharField(default="Number of students who have graduated without backlogs in the stipulated period", max_length=300)
    item_3 = models.CharField(default="Success Index ", max_length=300)

    lygm0_1 = models.CharField(max_length=250)
    lygm0_2 = models.CharField(max_length=250)
    lygm0_3 = models.CharField(max_length=250)

    lygm1_1 = models.CharField(max_length=250)
    lygm1_2 = models.CharField(max_length=250)
    lygm1_3 = models.CharField(max_length=250)

    lygm2_1 = models.CharField(max_length=250)
    lygm2_2 = models.CharField(max_length=250)
    lygm2_3 = models.CharField(max_length=250)

    class Meta:
        app_label = "mainApp"