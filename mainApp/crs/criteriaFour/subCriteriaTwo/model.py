from django.db import models
from mainApp.crs.criteriaFour.model import c_4

class c_4_2(models.Model):
    name = models.CharField(default="", max_length=250)
    criteria = models.ForeignKey(c_4, on_delete=models.CASCADE)
    class Meta:
        app_label = "mainApp"