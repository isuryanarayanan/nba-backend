from django.db import models
from mainApp.crs.mediaStorage.model import mediaStorage

CAYtype = (
    ('caym0','caym0'),
    ('caym1','caym1'),
    ('caym2','caym2'),
    ('caym3','caym3'),
    ('lygm0','lygm0'),
    ('lygm1','lygm1'),
    ('lygm2','lygm2'),
)
class cayCell(models.Model):
    cay = models.CharField(max_length=250, choices=CAYtype)
    pos = models.IntegerField(default=0, unique=True)
    media = models.ManyToManyField(mediaStorage, blank=True)
    
    class Meta:
        app_label = "mainApp"
        

class c_4(models.Model):
    name = models.CharField(default="Student Performance", max_length=250)
    
    #table 1
    item_1 = models.CharField(default="Sanctioned intake of the program (N)", max_length=300)
    item_2 = models.CharField(default="Total number of students admitted in first year minus number of students migrated to other programs/institutions, plus no. of students migrated to this program (N1)", max_length=300)
    item_3 = models.CharField(default="Number of students admitted in 2nd year in the same batch via lateral entry (N2)", max_length=300)
    item_4 = models.CharField(default="Separate division students, if applicable (N3)", max_length=300)
    item_5 = models.CharField(default="Total number of students admitted in the Program (N1 + N2 + N3)", max_length=300)

    caym0_1 = models.CharField(max_length=250)
    caym0_2 = models.CharField(max_length=250)
    caym0_3 = models.CharField(max_length=250)
    caym0_4 = models.CharField(max_length=250)
    caym0_5 = models.CharField(max_length=250)

    caym1_1 = models.CharField(max_length=250)
    caym1_2 = models.CharField(max_length=250)
    caym1_3 = models.CharField(max_length=250)
    caym1_4 = models.CharField(max_length=250)
    caym1_5 = models.CharField(max_length=250)

    caym2_1 = models.CharField(max_length=250)
    caym2_2 = models.CharField(max_length=250)
    caym2_3 = models.CharField(max_length=250)
    caym2_4 = models.CharField(max_length=250)
    caym2_5 = models.CharField(max_length=250)

    #table 2
    table2a = models.ManyToManyField(cayCell,related_name="table2a")
    table2b = models.ManyToManyField(cayCell,related_name="table2b")

    class Meta:
        app_label = "mainApp"