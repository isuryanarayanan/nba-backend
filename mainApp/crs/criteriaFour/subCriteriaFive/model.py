from django.db import models
from mainApp.crs.criteriaFour.model import c_4

class c_4_5(models.Model):

    name = models.CharField(default="Placement and higher studies and Entrepreneurship" ,max_length=250)
    subcriteria = models.ForeignKey(c_4, on_delete=models.CASCADE)
    item_1 = models.CharField(max_length=250)
    item_2 = models.CharField(max_length=250)
    item_3 = models.CharField(max_length=250)
    item_4 = models.CharField(max_length=250)
    item_5 = models.CharField(max_length=250)
    item_6 = models.CharField(max_length=250)
    
    lygm0_1 = models.CharField(max_length=250)
    lygm0_2 = models.CharField(max_length=250)
    lygm0_3 = models.CharField(max_length=250)
    lygm0_4 = models.CharField(max_length=250)
    lygm0_5 = models.CharField(max_length=250)
    lygm0_6 = models.CharField(max_length=250)

    lygm1_1 = models.CharField(max_length=250)
    lygm1_2 = models.CharField(max_length=250)
    lygm1_3 = models.CharField(max_length=250)
    lygm1_4 = models.CharField(max_length=250)
    lygm1_5 = models.CharField(max_length=250)
    lygm1_6 = models.CharField(max_length=250)

    lygm2_1 = models.CharField(max_length=250)
    lygm2_2 = models.CharField(max_length=250)
    lygm2_3 = models.CharField(max_length=250)
    lygm2_4 = models.CharField(max_length=250)
    lygm2_5 = models.CharField(max_length=250)
    lygm2_6 = models.CharField(max_length=250)

    class Meta:
        app_label = "mainApp"