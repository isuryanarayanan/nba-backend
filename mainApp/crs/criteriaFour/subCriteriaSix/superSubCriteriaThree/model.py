from django.db import models
from mainApp.crs.mediaStorage.model import mediaStorage
from mainApp.crs.criteriaFour.subCriteriaSix.model import c_4_6

class c_4_6_3(models.Model):

    name = models.CharField(default="" ,max_length=250)
    subcriteria = models.ForeignKey(c_4_6, on_delete=models.CASCADE)

    year_1_1 = models.CharField(default="" ,max_length=250)
    year_1_2 = models.CharField(default="" ,max_length=250)
    year_1_3 = models.CharField(default="" ,max_length=250)
    year_1_4 = models.CharField(default="" ,max_length=250)

    year_2_1 = models.CharField(default="" ,max_length=250)
    year_2_2 = models.CharField(default="" ,max_length=250)
    year_2_3 = models.CharField(default="" ,max_length=250)
    year_2_4 = models.CharField(default="" ,max_length=250)

    year_3_1 = models.CharField(default="" ,max_length=250)
    year_3_2 = models.CharField(default="" ,max_length=250)
    year_3_3 = models.CharField(default="" ,max_length=250)
    year_3_4 = models.CharField(default="" ,max_length=250)
    
    media = models.ManyToManyField(mediaStorage, blank=True)

    class Meta:
        app_label = "mainApp"