from django.db import models
from mainApp.crs.mediaStorage.model import mediaStorage
from mainApp.crs.criteriaFour.model import c_4

class c_4_6(models.Model):

    name = models.CharField(default="Professional activities" ,max_length=250)
    criteria = models.ForeignKey(c_4, on_delete=models.CASCADE)

    class Meta:
        app_label = "mainApp"