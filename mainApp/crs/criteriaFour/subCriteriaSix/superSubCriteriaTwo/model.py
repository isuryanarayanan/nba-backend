from django.db import models
from mainApp.crs.mediaStorage.model import mediaStorage
from mainApp.crs.criteriaFour.subCriteriaSix.model import c_4_6

class c_4_6_2(models.Model):

    name = models.CharField(default="" ,max_length=250)
    subcriteria = models.ForeignKey(c_4_6, on_delete=models.CASCADE)
    media = models.ManyToManyField(mediaStorage, blank=True)

    class Meta:
        app_label = "mainApp"