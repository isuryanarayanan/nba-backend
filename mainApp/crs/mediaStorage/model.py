from django.db import models

class mediaStorage(models.Model):
    
    data = models.FileField(upload_to="media-storage/", max_length=200, null=True)
    
    class Meta:
        app_label = "mainApp"