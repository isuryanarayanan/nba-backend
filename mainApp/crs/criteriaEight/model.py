from django.db import models

class c_8(models.Model):

    name = models.CharField(
        default="First Year Academics", max_length=250)

    def __str__(self):
        return self.name
    
    class Meta:
        app_label = "mainApp"


