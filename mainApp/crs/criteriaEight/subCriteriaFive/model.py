from django.db import models

from mainApp.crs.criteriaEight.model import c_8
from mainApp.crs.mediaStorage.model import mediaStorage

class c_8_5(models.Model):
    name = models.CharField(
        default="8.5", max_length=250)
    criteria = models.ForeignKey(c_8, on_delete=models.CASCADE)

    class Meta:
        app_label = "mainApp"