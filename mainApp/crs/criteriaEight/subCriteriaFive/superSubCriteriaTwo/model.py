from django.db import models

from mainApp.crs.criteriaEight.subCriteriaFive.model import c_8_5

class c_8_5_2(models.Model):
    name = models.CharField(
        default="Actions taken based on the results of evaluation of relevant POs and PSOs", max_length=250)
    subcriteria = models.ForeignKey(c_8_5, on_delete=models.CASCADE)
    cfym0 = models.FileField(upload_to="cr8/sub5/sub2/", max_length=200)
    cfym1 = models.FileField(upload_to="cr8/sub5/sub2/", max_length=200)
    cfym2 = models.FileField(upload_to="cr8/sub5/sub2/", max_length=200)
    class Meta:
        app_label = "mainApp"
        