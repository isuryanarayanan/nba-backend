from django.db import models

from mainApp.crs.criteriaEight.subCriteriaFive.model import c_8_5
from mainApp.crs.mediaStorage.model import mediaStorage

class c_8_5_1(models.Model):
    name = models.CharField(
        default="Indicate results of evaluation of each relevant PO and/or PSO if applicable", max_length=250)
    subcriteria = models.ForeignKey(c_8_5, on_delete=models.CASCADE)
    description_file = models.FileField(upload_to="cr8/sub5/sub1/", max_length=200)
    media = models.ManyToManyField(mediaStorage, blank=True)
    class Meta:
        app_label = "mainApp"
