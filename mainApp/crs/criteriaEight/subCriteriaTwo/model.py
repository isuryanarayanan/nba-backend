from django.db import models

from mainApp.crs.criteriaEight.model import c_8
from mainApp.crs.proofStorage.model import proofStorage

class c_8_2(models.Model):
    name = models.CharField(
        default="Qualification of Faculty Teaching First Year Common Courses", max_length=250)
    criteria = models.ForeignKey(c_8, on_delete=models.CASCADE)

    #cay m0
    caym0_1 = models.CharField(max_length=250)
    caym0_proof_1 = models.FileField(upload_to="cr8/sub2/caym0/ ", max_length=250) 
    caym0_2 = models.CharField(max_length=250)
    caym0_proof_2 = models.FileField(upload_to="cr8/sub2/caym0/", max_length=250) 
    caym0_3 = models.CharField(max_length=250)

    #cay m1
    caym1_1 = models.CharField(max_length=250)
    caym1_proof_1 = models.FileField(upload_to="cr8/sub2/caym1/", max_length=250) 
    caym1_2 = models.CharField(max_length=250)
    caym1_proof_2 = models.FileField(upload_to="cr8/sub2/caym1/", max_length=250) 
    caym1_3 = models.CharField(max_length=250)

    #cay m2
    caym2_1 = models.CharField(max_length=250)
    caym2_proof_1 = models.FileField(upload_to="cr8/sub2/caym2/", max_length=250) 
    caym2_2 = models.CharField(max_length=250)
    caym2_proof_2 = models.FileField(upload_to="cr8/sub2/caym2/", max_length=250) 
    caym2_3 = models.CharField(max_length=250)

    class Meta:
        app_label = "mainApp"