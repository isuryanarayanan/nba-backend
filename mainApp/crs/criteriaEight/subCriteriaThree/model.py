from django.db import models

from mainApp.crs.criteriaEight.model import c_8
from mainApp.crs.proofStorage.model import proofStorage
from mainApp.crs.sampleStorage.model import sampleStorage


class c_8_3(models.Model):
    name = models.CharField(
        default="First Year Academic Performance", max_length=250)
    criteria = models.ForeignKey(c_8, on_delete=models.CASCADE)

    #cay m0
    caym0_1 = models.CharField(max_length=250)
    caym0_2 = models.CharField(max_length=250)
    caym0_3 = models.CharField(max_length=250)
    caym0_4 = models.CharField(max_length=250)

    #cay m1
    caym1_1 = models.CharField(max_length=250)
    caym1_2 = models.CharField(max_length=250)
    caym1_3 = models.CharField(max_length=250)
    caym1_4 = models.CharField(max_length=250)

    #cay m2
    caym2_1 = models.CharField(max_length=250)
    caym2_2 = models.CharField(max_length=250)
    caym2_3 = models.CharField(max_length=250)
    caym2_4 = models.CharField(max_length=250)

    proof = models.ManyToManyField(proofStorage)
    
    sample = models.ManyToManyField(sampleStorage)
    class Meta:
        app_label = "mainApp"