from django.db import models

from mainApp.crs.criteriaEight.model import c_8
from mainApp.crs.mediaStorage.model import mediaStorage

class c_8_1(models.Model):
    name = models.CharField(
        default="First Year Student-Faculty Ratio", max_length=250)

    criteria = models.ForeignKey(c_8, on_delete=models.CASCADE)

    #cay m0
    caym0_1 = models.CharField(default="Number of students (approved intake strength)", max_length=250)
    caym0_2 = models.CharField(default="NNumber of faculty members (considering fractional load)", max_length=250)
    caym0_3 = models.CharField(default="FYSFR", max_length=250)
    caym0_4 = models.FileField(upload_to="cr8/sub1/caym0/", max_length=250) 

    #cay m1
    caym1_1 = models.CharField(default="Number of students (approved intake strength)", max_length=250)
    caym1_2 = models.CharField(default="NNumber of faculty members (considering fractional load)", max_length=250)
    caym1_3 = models.CharField(default="FYSFR", max_length=250)
    caym1_4 = models.FileField(upload_to="cr8/sub1/caym1/", max_length=250) 

    #cay m2
    caym2_1 = models.CharField(default="Number of students (approved intake strength)", max_length=250)
    caym2_2 = models.CharField(default="NNumber of faculty members (considering fractional load)", max_length=250)
    caym2_3 = models.CharField(default="FYSFR", max_length=250)
    caym2_4 = models.FileField(upload_to="cr8/sub1/caym2/", max_length=250) 

    class Meta:
        app_label = "mainApp"