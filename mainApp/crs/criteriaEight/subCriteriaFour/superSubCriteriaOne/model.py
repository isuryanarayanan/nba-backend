from django.db import models

from mainApp.crs.criteriaEight.subCriteriaFour.model import c_8_4
from mainApp.crs.mediaStorage.model import mediaStorage

class c_8_4_1(models.Model):
    name = models.CharField(
        default="Describe the assessment processes used to gather the data upon which the evaluation of Course Outcomes of first year is done", max_length=250)
    subcriteria = models.ForeignKey(c_8_4, on_delete=models.CASCADE)
    description_file = models.FileField(upload_to="cr8/sub4/sub1/", max_length=200)
    media = models.ManyToManyField(mediaStorage, blank=True)
    class Meta:
        app_label = "mainApp"