from django.db import models

from mainApp.crs.criteriaEight.model import c_8
from mainApp.crs.mediaStorage.model import mediaStorage

class c_8_4(models.Model):
    name = models.CharField(
        default="Attainment of Course Outcomes of first year courses", max_length=250)
    criteria = models.ForeignKey(c_8, on_delete=models.CASCADE)
    description_file = models.FileField(upload_to="cr8/sub4/", max_length=200)
    media = models.ManyToManyField(mediaStorage, blank=True)
    class Meta:
        app_label = "mainApp"