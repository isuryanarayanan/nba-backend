from django.db import models

class sampleStorage(models.Model):
    
    data = models.FileField(upload_to="sample-storage/", max_length=200, null=True)
    
    class Meta:
        app_label = "mainApp"