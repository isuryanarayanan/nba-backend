from django.db import models

class proofStorage(models.Model):
    
    data = models.FileField(upload_to="proof-storage/", max_length=200, null=True)
    
    class Meta:
        app_label = "mainApp"