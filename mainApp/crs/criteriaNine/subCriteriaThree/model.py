from django.db import models

from mainApp.crs.criteriaNine.model import c_9
from mainApp.crs.proofStorage.model import proofStorage
from mainApp.crs.sampleStorage.model import sampleStorage

class programExitSurvey(models.Model):
    data =  models.FileField(upload_to="cr9/sub2/programExitSurvey/", max_length=250) 
    class Meta:
        app_label = "mainApp"

class feedbackFacilities(models.Model):
    data =  models.FileField(upload_to="cr9/sub2/feedbackFacilities/", max_length=250) 
    class Meta:
        app_label = "mainApp"


class feedbackParents(models.Model):
    data =  models.FileField(upload_to="cr9/sub2/feedbackParents/", max_length=250) 
    class Meta:
        app_label = "mainApp"

class addContent(models.Model):
    data1 = models.CharField(default="Type of Feedback", max_length=250)
    data2 = models.CharField(default="Collection ethod and Analysis", max_length=250)
    data3 = models.CharField(default="Reward/corrective Action", max_length=250)
    data4 = models.CharField(default="Effectiveness", max_length=250)

    class Meta:
        app_label = "mainApp"

class c_9_3(models.Model):
    name = models.CharField(
        default="Feedback on facilities", max_length=250)
    criteria = models.ForeignKey(c_9, on_delete=models.CASCADE)
    content = models.ManyToManyField(addContent)
    
    description_file =  models.FileField(upload_to="cr9/sub3/", max_length=250) 

    program_exit_survey = models.ManyToManyField(programExitSurvey)
    feedback_facilities = models.ManyToManyField(feedbackFacilities)
    feedback_parent = models.ManyToManyField(feedbackParents)

    proof = models.ManyToManyField(proofStorage, blank=True)
    
    sample = models.ManyToManyField(sampleStorage, blank=True)
    class Meta:
        app_label = "mainApp"