from django.db import models

from mainApp.crs.criteriaNine.model import c_9
from mainApp.crs.mediaStorage.model import mediaStorage
from mainApp.crs.proofStorage.model import proofStorage
from mainApp.crs.sampleStorage.model import sampleStorage

class appendixA(models.Model):
    data = models.FileField(upload_to="cr9/sub1/appendixa/", max_length=250) 
    class Meta:
        app_label = "mainApp"

class appendixB(models.Model):
    data = models.FileField(upload_to="cr9/sub1/appendixb/", max_length=250) 
    class Meta:
        app_label = "mainApp"

class c_9_1(models.Model):
    name = models.CharField(
        default="Mentoring system to help at individual level", max_length=250)

    criteria = models.ForeignKey(c_9, on_delete=models.CASCADE)
    description_file1 = models.FileField(upload_to="cr9/sub1/ ", max_length=250) 
    description_file2 = models.FileField(upload_to="cr9/sub1/ ", max_length=250) 

    appendixa = models.ManyToManyField(appendixA)
    appendixb = models.ManyToManyField(appendixB)

    sample = models.ManyToManyField(sampleStorage, blank=True)
    proof = models.ManyToManyField(proofStorage, blank=True)
    class Meta:
        app_label = "mainApp"