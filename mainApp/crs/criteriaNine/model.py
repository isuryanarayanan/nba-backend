from django.db import models


class c_9(models.Model):

    name = models.CharField(
        default="Student Support Systems", max_length=250)

    def __str__(self):
        return self.name
    
    class Meta:
        app_label = "mainApp"


