from django.db import models

from mainApp.crs.criteriaNine.model import c_9
from mainApp.crs.mediaStorage.model import mediaStorage
from mainApp.crs.proofStorage.model import proofStorage
from mainApp.crs.sampleStorage.model import sampleStorage

class courseExitSurvey(models.Model):
    data =  models.FileField(upload_to="cr9/sub2/courseExitSurvey/", max_length=250) 
    class Meta:
        app_label = "mainApp"

class teachingEffectiveness(models.Model):
    data =  models.FileField(upload_to="cr9/sub2/teachingEffectiveness/", max_length=250) 
    class Meta:
        app_label = "mainApp"



class c_9_2(models.Model):
    name = models.CharField(default="Feedback analysis and reward /corrective measures taken, if any", max_length=250)
    criteria = models.ForeignKey(c_9, on_delete=models.CASCADE)
    description_file = models.FileField(upload_to="cr9/sub2/ ", max_length=250) 
    course_exit = models.ManyToManyField(courseExitSurvey)
    template_effectiveness = models.ManyToManyField(teachingEffectiveness)
    sample = models.ManyToManyField(sampleStorage, blank=True)
    proof = models.ManyToManyField(proofStorage, blank=True)
    
    class Meta:
        app_label = "mainApp"