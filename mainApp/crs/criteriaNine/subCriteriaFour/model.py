from django.db import models

from mainApp.crs.criteriaNine.model import c_9
from mainApp.crs.proofStorage.model import proofStorage
from mainApp.crs.sampleStorage.model import sampleStorage

class c_9_4(models.Model):
    name = models.CharField(default="Self-Learning", max_length=250)
    criteria = models.ForeignKey(c_9, on_delete=models.CASCADE)
    description_file1 = models.FileField(upload_to="cr9/sub4/", max_length=200)
    description_file2 = models.FileField(upload_to="cr9/sub4/", max_length=200)
    description_file3 = models.FileField(upload_to="cr9/sub4/", max_length=200)
    description_file4 = models.FileField(upload_to="cr9/sub4/", max_length=200)
    description_file5 = models.FileField(upload_to="cr9/sub4/", max_length=200)
    sample = models.ManyToManyField(sampleStorage, blank=True)
    proof = models.ManyToManyField(proofStorage, blank=True)
    class Meta:
        app_label = "mainApp"