from django.db import models

from mainApp.crs.criteriaSix.model import c_6
from mainApp.crs.sampleStorage.model import sampleStorage
from mainApp.crs.proofStorage.model import proofStorage

class addContent2(models.Model):
    data1 = models.CharField(default='srno', max_length=250)
    data2 = models.CharField(default='Facility Name', max_length=250)
    data3 = models.CharField(default='Details', max_length=250)
    data4 = models.CharField(default='Reason(s) for creating facility', max_length=250)
    data5 = models.CharField(default='Utilization', max_length=250)
    data6 = models.CharField(default='Areas in which students are expected to have enhanced learning', max_length=250)
    data7 = models.CharField(default='Relevance to POs/PSOs', max_length=250)
    proof = models.ManyToManyField(proofStorage, blank=True)

    def __str__(self):
        return f'{self.data1}'

    class Meta:
        app_label = "mainApp"
        
class c_6_2(models.Model):
    name = models.CharField(
        default="Additional facilities created for the quality of learning experience in laboratries", max_length=250)
    criteria = models.ForeignKey(c_6, on_delete=models.CASCADE)
    content = models.ManyToManyField(addContent2, blank=True)
    sample = models.ManyToManyField(sampleStorage, blank=True)
    class Meta:
        app_label = "mainApp"