from django.db import models

from mainApp.crs.criteriaSix.model import c_6
from mainApp.crs.proofStorage.model import proofStorage

class addContent1(models.Model):
    data1 = models.CharField(default='srno', max_length=250)
    data2 = models.CharField(default='Name of Laboratories', max_length=250)
    data3 = models.CharField(default='No.of students per setup(Batch Size)', max_length=250)
    data4 = models.CharField(default='Name of important equipment', max_length=250)
    data5 = models.CharField(default='Weekly utilization status(all the courses for which the lab is utilized)', max_length=250)
    data6 = models.CharField(default='Name of the technical staff', max_length=250)
    data7 = models.CharField(default='Designation', max_length=250)
    data8 = models.CharField(default='Qualification', max_length=250)
    proof = models.ManyToManyField(proofStorage, blank=True)
    
    def __str__(self):
        return f'{self.data1}'

    class Meta:
        app_label = "mainApp"

class c_6_1(models.Model):
    name = models.CharField(
        default="Adequate and well equipped laboratories, and technical manpower", max_length=250)
    content = models.ManyToManyField(addContent1, blank=True)
    criteria = models.ForeignKey(c_6, on_delete=models.CASCADE)
    
    class Meta:
        app_label = "mainApp"