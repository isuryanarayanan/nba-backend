from django.db import models

from mainApp.crs.criteriaSix.model import c_6
from mainApp.crs.sampleStorage.model import sampleStorage
from mainApp.crs.proofStorage.model import proofStorage


class c_6_3(models.Model):
    name = models.CharField(
        default="Laboratories maintenance and overall ambiance", max_length=250)
    criteria = models.ForeignKey(c_6, on_delete=models.CASCADE)
    description_file = models.FileField(upload_to="cr6/sub3/", max_length=200)
    sample = models.ManyToManyField(sampleStorage, blank=True)
    proof = models.ManyToManyField(proofStorage, blank=True)
    class Meta:
        app_label = "mainApp"
        