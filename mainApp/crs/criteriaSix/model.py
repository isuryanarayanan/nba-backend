from django.db import models

class c_6(models.Model):

    name = models.CharField(default="Facilities and Technical Support", max_length=250)

    class Meta:
        app_label = "mainApp"