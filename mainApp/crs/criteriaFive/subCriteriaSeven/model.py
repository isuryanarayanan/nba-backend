from django.db import models

from mainApp.crs.criteriaFive.model import c_5

class c_5_7addContent(models.Model):
    data1 = models.CharField(default="year", max_length=250)
    data2 = models.CharField(default="project title", max_length=250)
    data3 = models.CharField(default="funding agency", max_length=250)
    data4 = models.CharField(default="duration", max_length=250)
    data5 = models.CharField(default="funding amount", max_length=250)
    class Meta:
        app_label = "mainApp"

class c_5_7(models.Model):
    name = models.CharField(
        default="Research and Developent", max_length=250)
    criteria = models.ForeignKey(c_5, on_delete=models.CASCADE)
    
    class Meta:
        app_label = "mainApp"