from django.db import models 
from mainApp.crs.criteriaFive.subCriteriaSeven.model import c_5_7
from mainApp.crs.mediaStorage.model import mediaStorage

class c_5_7_1(models.Model):
    name = models.CharField(default="Academic Research", max_length=250)
    subcriteria = models.ForeignKey(c_5_7, on_delete=models.CASCADE)
    description_file = models.FileField(upload_to="cr5/sub7/", max_length=200)
    media = models.ManyToManyField(mediaStorage, blank=True)

    class Meta:
        app_label = "mainApp"