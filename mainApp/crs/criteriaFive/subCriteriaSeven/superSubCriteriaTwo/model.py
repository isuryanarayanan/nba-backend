from django.db import models 
from mainApp.crs.criteriaFive.subCriteriaSeven.model import c_5_7, c_5_7addContent
from mainApp.crs.mediaStorage.model import mediaStorage


class c_5_7_2(models.Model):
    name = models.CharField(default="Sponsored Research", max_length=250)
    subcriteria = models.ForeignKey(c_5_7, on_delete=models.CASCADE)
    content_2 = models.ManyToManyField(c_5_7addContent)
    media = models.ManyToManyField(mediaStorage, blank=True)

    class Meta:
        app_label = "mainApp"