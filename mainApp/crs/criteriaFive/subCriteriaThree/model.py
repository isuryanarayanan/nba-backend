from django.db import models

from mainApp.crs.criteriaFive.model import c_5


class c_5_3(models.Model):
    name = models.CharField(
        default="Faculty Qualification", max_length=250)
    criteria = models.ForeignKey(c_5, on_delete=models.CASCADE)

    caym0_1 = models.CharField(max_length=250)
    caym1_1 = models.CharField(max_length=250)
    caym2_1 = models.CharField(max_length=250)
    
    caym0_2 = models.CharField(max_length=250)
    caym1_2 = models.CharField(max_length=250)
    caym2_2 = models.CharField(max_length=250)

    caym0_3 = models.CharField(max_length=250)
    caym1_3 = models.CharField(max_length=250)
    caym2_3 = models.CharField(max_length=250)

    caym0_4 = models.CharField(max_length=250)
    caym1_4 = models.CharField(max_length=250)
    caym2_4 = models.CharField(max_length=250)
    average_assessment_4 = models.CharField(max_length=250)
    class Meta:
        app_label = "mainApp"