from django.db import models
from mainApp.crs.mediaStorage.model import mediaStorage
class c_5(models.Model):

    name = models.CharField(default="Faculty Information and Contributions", max_length=250)
    description = models.TextField()
    media = models.ManyToManyField(mediaStorage, blank=True)
    
    class Meta:
        app_label = "mainApp"