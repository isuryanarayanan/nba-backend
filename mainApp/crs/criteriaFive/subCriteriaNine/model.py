from django.db import models

from mainApp.crs.criteriaFive.model import c_5
from mainApp.crs.mediaStorage.model import mediaStorage

class c_5_9(models.Model):
    name = models.CharField(default="Visiting/Adjunct/Emeritus Faculty", max_length=250)
    criteria = models.ForeignKey(c_5, on_delete=models.CASCADE)
    description_file = models.FileField(upload_to="cr5/sub9/", max_length=200)
    media = models.ManyToManyField(mediaStorage, blank=True)

    class Meta:
        app_label = "mainApp"