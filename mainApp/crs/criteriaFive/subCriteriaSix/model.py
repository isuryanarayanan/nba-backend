from django.db import models

from mainApp.crs.criteriaFive.model import c_5

class c5addContent(models.Model):
    data1 = models.CharField(default="Name of the Faculty", max_length=250)
    data2 = models.CharField(default="CAY M0", max_length=250)
    data3 = models.CharField(default="CAY M1", max_length=250)
    data4 = models.CharField(default="CAY M2", max_length=250)
    
    class Meta:
        app_label = "mainApp"

class c_5_6(models.Model):
    name = models.CharField(
        default="Faculty as participants in Faculty development/training activities/STTPs", max_length=250)
    criteria = models.ForeignKey(c_5, on_delete=models.CASCADE)
    content = models.ManyToManyField(c5addContent)
    class Meta:
        app_label = "mainApp"