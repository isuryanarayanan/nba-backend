from django.db import models

from mainApp.crs.criteriaOne.model import c_1
from mainApp.crs.mediaStorage.model import mediaStorage

class cell(models.Model):
    cellData = models.CharField(default="cell data" ,max_length=250)
    mission = models.IntegerField(default=0)
    pso = models.IntegerField(default=0)

    def __str__(self):
        return f'{self.mission}-{self.pso}'
    class Meta:
        app_label = "mainApp"

class c_1_5(models.Model):
    name = models.CharField(default="Establish consistency of PEOs with Mission of the Department", max_length=250)
    criteria = models.ForeignKey(c_1, on_delete=models.CASCADE)
    number_missions = models.IntegerField()
    number_pso = models.IntegerField()
    cells = models.ManyToManyField(cell)
    media = models.ManyToManyField(mediaStorage, blank=True)
    class Meta:
        app_label = "mainApp"