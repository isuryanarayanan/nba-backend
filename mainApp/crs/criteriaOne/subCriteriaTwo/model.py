from django.db import models

from mainApp.crs.criteriaOne.model import c_1
from mainApp.crs.mediaStorage.model import mediaStorage

class c_1_2(models.Model):
    name = models.CharField(
        default="State the Program Educational Objectives", max_length=250)
    criteria = models.ForeignKey(c_1, on_delete=models.CASCADE)
    description = models.TextField()
    media = models.ManyToManyField(mediaStorage, blank=True)
    class Meta:
        app_label = "mainApp"