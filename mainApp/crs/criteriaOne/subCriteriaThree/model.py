from django.db import models

from mainApp.crs.criteriaOne.model import c_1
from mainApp.crs.mediaStorage.model import mediaStorage


class c_1_3(models.Model):
    name = models.CharField(
        default="Indicate where the Vision, Mission and PEOs are published and disseminated among stakeholders", max_length=250)
    criteria = models.ForeignKey(c_1, on_delete=models.CASCADE)
    description_file = models.FileField(upload_to="cr1/sub3/", max_length=200)
    media = models.ManyToManyField(mediaStorage, blank=True)
    class Meta:
        app_label = "mainApp"
        