from django.db import models

class c_1(models.Model):

    name = models.CharField(
        default="Vision, Mission and Program Educational Objectives", max_length=250)

    def __str__(self):
        return self.name
    
    class Meta:
        app_label = "mainApp"


