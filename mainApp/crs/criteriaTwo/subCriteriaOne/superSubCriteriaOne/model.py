from mainApp.crs.criteriaTwo.subCriteriaOne.model import c_2_1
from django.db import models

class CurricularGaps(models.Model):
    stream = models.CharField(default="stream", max_length=250)
    subject_with_gap = models.CharField(default="subject_with_gap", max_length=250)
    workshop_planned = models.CharField(default="workshop_planned", max_length=250)
    po = models.CharField(default="po", max_length=250)
    pso = models.CharField(default="pso", max_length=250)

    class Meta:
        app_label = "mainApp"

class c_2_1_1(models.Model):
    subcriteria = models.ForeignKey(c_2_1, on_delete=models.CASCADE)

    name = models.CharField(
        default="State the process for designing the program curriculum", max_length=250)
    description_file = models.FileField(upload_to="cr2/sub1/sub1/", max_length=200)
    curricular_gaps = models.ManyToManyField(CurricularGaps)
    
    def __str__(self):
        return self.name
    
    class Meta:
        app_label = "mainApp"