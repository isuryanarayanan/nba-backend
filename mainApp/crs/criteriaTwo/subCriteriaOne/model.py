from mainApp.crs.criteriaTwo.model import c_2
from mainApp.crs.sampleStorage.model import sampleStorage
from mainApp.crs.proofStorage.model import proofStorage
from django.db import models

class c_2_1(models.Model):
    criteria = models.ForeignKey(c_2, on_delete=models.CASCADE)

    name = models.CharField(
        default="2.2", max_length=250)
    sample = models.ManyToManyField(sampleStorage)
    proof = models.ManyToManyField(proofStorage)

    def __str__(self):
        return self.name
    
    class Meta:
        app_label = "mainApp"