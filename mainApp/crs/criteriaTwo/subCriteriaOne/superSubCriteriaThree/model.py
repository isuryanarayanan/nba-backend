from mainApp.crs.criteriaTwo.subCriteriaOne.model import c_2_1
from django.db import models

class c_2_1_3(models.Model):
    subcriteria = models.ForeignKey(c_2_1, on_delete=models.CASCADE)

    name = models.CharField(
        default="State the components of the curriculum", max_length=250)
    
    basic_sciences1 = models.CharField(default="basic sciences | Curriculum Content" ,max_length=250)
    basic_sciences2 = models.CharField(default="basic sciences | Contact Hours" ,max_length=250)
    basic_sciences3 = models.CharField(default="basic sciences | Total Credits" ,max_length=250)
    
    engineering_sciences1 = models.CharField(default="engineering sciences | Curriculum Content" ,max_length=250)
    engineering_sciences2 = models.CharField(default="engineering sciences | Contact Hours" ,max_length=250)
    engineering_sciences3 = models.CharField(default="engineering sciences | Total Credits" ,max_length=250)
    
    hum_and_social_sciences1 = models.CharField(default="humanities and social sciences | Curriculum Content" ,max_length=250)
    hum_and_social_sciences2 = models.CharField(default="humanities and social sciences | Contact Hours" ,max_length=250)
    hum_and_social_sciences3 = models.CharField(default="humanities and social sciences | Total Credits" ,max_length=250)
    
    program_core1 = models.CharField(default="program core | Curriculum Content" ,max_length=250)
    program_core2 = models.CharField(default="program core | Contact Hours" ,max_length=250)
    program_core3 = models.CharField(default="program core | Total Credits" ,max_length=250)
    
    program_electives1 = models.CharField(default="Program electives | Curriculum Content" ,max_length=250)
    program_electives2 = models.CharField(default="Program electives | Contact Hours" ,max_length=250)
    program_electives3 = models.CharField(default="Program electives | Total Credits" ,max_length=250)
    
    open_electives1 = models.CharField(default="open electives | Curriculum Content" ,max_length=250)
    open_electives2 = models.CharField(default="open electives | Contact Hours" ,max_length=250)
    open_electives3 = models.CharField(default="open electives | Total Credits" ,max_length=250)
    
    projects1 = models.CharField(default="projects | Curriculum Content" ,max_length=250)
    projects2 = models.CharField(default="projects | Contact Hours" ,max_length=250)
    projects3 = models.CharField(default="projects | Total Credits" ,max_length=250)
    
    internships_seminars1 = models.CharField(default="internships and seminars | Curriculum Content" ,max_length=250)
    internships_seminars2 = models.CharField(default="internships and seminars | Contact Hours" ,max_length=250)
    internships_seminars3 = models.CharField(default="internships and seminars | Total Credits" ,max_length=250)
    
    any_other1 = models.CharField(default="any other | Curriculum Content" ,max_length=250)
    any_other2 = models.CharField(default="any other | Contact Hours" ,max_length=250)
    any_other3 = models.CharField(default="any other | Total Credits" ,max_length=250)
    
    total = models.CharField(default="total | Total Credits", max_length=250)
    
    def __str__(self):
        return self.name
    
    class Meta:
        app_label = "mainApp"