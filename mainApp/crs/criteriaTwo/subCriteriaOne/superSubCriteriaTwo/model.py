from mainApp.crs.criteriaTwo.subCriteriaOne.model import c_2_1
from django.db import models

class yearData(models.Model):
    year = models.CharField(default="2017-18", max_length=250)

    slno = models.CharField(default="slno", max_length=250)
    gap = models.CharField(default="gap", max_length=250)
    action_taken = models.CharField(default="action taken", max_length=250)
    dmy = models.CharField(default="date month year", max_length=250)
    resource_person = models.CharField(default="resource person", max_length=250)
    percentage = models.CharField(default="percentage", max_length=250)
    relevant_po = models.CharField(default="relevant po's", max_length=250)
    class Meta:
        app_label = "mainApp"


class c_2_1_2(models.Model):
    subcriteria = models.ForeignKey(c_2_1, on_delete=models.CASCADE)

    name = models.CharField(
        default="Structure of the Curriculum", max_length=250)
    
    course_code1 = models.CharField(default="course code 1", max_length=200)
    course_code2 = models.CharField(default="course code 2", max_length=200)

    course_title1 = models.CharField(default="course title 1", max_length=200)
    course_title2 = models.CharField(default="course title 2", max_length=200)

    #total number of contact hours
    lecture1 = models.CharField(default="lecture 1", max_length=200)
    lecture2 = models.CharField(default="lecture 2", max_length=200)
    total_lecture = models.CharField(default="lecture total", max_length=200)
        
    tutorial1 = models.CharField(default="tutorial 1", max_length=200)
    tutorial2 = models.CharField(default="tutorial 2", max_length=200)
    total_tutorial = models.CharField(default="tutorial total", max_length=200)
    
    practical1 = models.CharField(default="practical 1", max_length=200)
    practical2 = models.CharField(default="practical 2", max_length=200)
    total_practical = models.CharField(default="practical total", max_length=200)
       
    hours1 = models.CharField(default="total hours 1", max_length=200)
    hours2 = models.CharField(default="total hours 2", max_length=200)
    total_hours = models.CharField(default="total hours", max_length=200)

    credits1 = models.CharField(default="credits 1", max_length=200)
    credits2 = models.CharField(default="credits 2", max_length=200)
    credits3 = models.CharField(default="credits 3", max_length=200)

    year = models.ManyToManyField(yearData)

    def __str__(self):
        return self.name
    
    class Meta:
        app_label = "mainApp"
