from mainApp.crs.criteriaTwo.subCriteriaOne.model import c_2_1
from django.db import models

class c_2_1_4(models.Model):

    subcriteria = models.ForeignKey(c_2_1, on_delete=models.CASCADE)
    name = models.CharField(default="State the process used to identify extent of compliance of the curriculum for attaining the Program Outcomes and Program Specific Outcomes as mentioned in Annexure I", max_length=250)
    description = models.TextField()

    def __str__(self):
        return self.name
    
    class Meta:
        app_label = "mainApp"