from django.db import models

class c_2(models.Model):

    name = models.CharField(
        default="Program Curriculum and Teaching –Learning Processes", max_length=250)

    def __str__(self):
        return self.name

    class Meta:
        app_label = "mainApp"
