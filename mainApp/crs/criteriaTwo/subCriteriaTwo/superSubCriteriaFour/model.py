from django.db import models
from mainApp.crs.criteriaTwo.subCriteriaTwo.model import c_2_2
class c_2_2_4(models.Model):
    subcriteria = models.ForeignKey(c_2_2, on_delete=models.CASCADE)
    name = models.CharField(default="Initiatives related to industry interaction", max_length=250)
    description_file = models.FileField(upload_to="cr2/sub2/", max_length=200)

    class Meta:
        app_label = "mainApp"
