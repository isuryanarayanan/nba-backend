from django.db import models
from mainApp.crs.criteriaTwo.model import c_2
from mainApp.crs.proofStorage.model import proofStorage
from mainApp.crs.sampleStorage.model import sampleStorage

class c_2_2(models.Model):
    name = models.CharField(default="Teaching-Learning Processes", max_length=250)
    criteria = models.ForeignKey(c_2, on_delete=models.CASCADE)
    proofs = models.ManyToManyField(proofStorage)
    samples = models.ManyToManyField(sampleStorage)
    
    def __str__(self):
        return self.name
    
    class Meta:
        app_label = "mainApp"