from django.db import models 
from mainApp.crs.criteriaTen.model import c_10, c10addContent1
from mainApp.crs.mediaStorage.model import mediaStorage

class c_10_3(models.Model):

    criteria = models.ForeignKey(c_10, on_delete=models.CASCADE) 
    name = models.CharField(default="Improvement in Placement, Higher Studies and Entrepreneurship", max_length=250)

    cfym0 = models.ManyToManyField(c10addContent1, related_name="cfym0")     
    cfym1 = models.ManyToManyField(c10addContent1, related_name="cfym1")
    cfym2 = models.ManyToManyField(c10addContent1, related_name="cfym2")
    cfym3 = models.ManyToManyField(c10addContent1, related_name="cfym3")
    
    class Meta:
        app_label = "mainApp"