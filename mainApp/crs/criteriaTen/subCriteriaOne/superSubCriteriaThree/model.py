from django.db import models 
from mainApp.crs.criteriaTen.subCriteriaOne.model import c_10_1
from mainApp.crs.mediaStorage.model import mediaStorage

class c_10_1_3(models.Model):
    name = models.CharField(default="Governing body, administrative setup, functions of various bodies, service rules, procedures, recruitment and promotional policies", max_length=250)
    subcriteria = models.ForeignKey(c_10_1, on_delete=models.CASCADE)
    description_file = models.FileField(upload_to="cr10/sub3/", max_length=200)
    media = models.ManyToManyField(mediaStorage)

    class Meta:
        app_label = "mainApp"