from django.db import models 
from mainApp.crs.criteriaTen.subCriteriaOne.model import c_10_1
from mainApp.crs.mediaStorage.model import mediaStorage

class c_10_1_6(models.Model):
    name = models.CharField(default="Transparency and availability of correct/unambiguous information in public domain", max_length=250)
    subcriteria = models.ForeignKey(c_10_1, on_delete=models.CASCADE)
    description_file = models.FileField(upload_to="cr10/sub6/", max_length=200)
    media = models.ManyToManyField(mediaStorage)

    class Meta:
        app_label = "mainApp"