from django.db import models 
from mainApp.crs.criteriaTen.model import c_10

class c_10_1(models.Model):
    criteria = models.ForeignKey(c_10, on_delete=models.CASCADE) 
    name = models.CharField(default="Organization, Governance and Transparency", max_length=250)

    class Meta:
        app_label = "mainApp"