from django.db import models 
from mainApp.crs.criteriaTen.subCriteriaOne.model import c_10_1
from mainApp.crs.mediaStorage.model import mediaStorage

class c_10_1_1(models.Model):
    name = models.CharField(default="State the Vision and Mission of the Institute", max_length=250)
    subcriteria = models.ForeignKey(c_10_1, on_delete=models.CASCADE)
    description_file = models.FileField(upload_to="cr10/sub1/", max_length=200)
    media = models.ManyToManyField(mediaStorage)

    class Meta:
        app_label = "mainApp"