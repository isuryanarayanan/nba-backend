from django.db import models 
from mainApp.crs.mediaStorage.model import mediaStorage
class c10addContent1(models.Model):
    data1 = models.CharField(default="Fee", max_length=250)
    data2 = models.CharField(default="Govt", max_length=250)
    data3 = models.CharField(default="Grant(s)", max_length=250)
    data4 = models.CharField(default="Other Sources (specify)", max_length=250)
    data5 = models.CharField(default="Recurring including Salaries", max_length=250)
    data6 = models.CharField(default="Non recurring", max_length=250)
    data7 = models.CharField(default="Special Projects/Any other,specify", max_length=250)
    data8 = models.CharField(default="Expenditure per student", max_length=250)
    media = models.ManyToManyField(mediaStorage)
    class Meta:
        app_label = "mainApp"

class c_10(models.Model):
    
    name = models.CharField(default="Governance, Institutional Support and Financial Resources", max_length=250)

    class Meta:
        app_label = "mainApp"