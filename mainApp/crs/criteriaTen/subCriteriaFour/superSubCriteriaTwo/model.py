from django.db import models 
from mainApp.crs.criteriaTen.subCriteriaFour.model import c_10_4
from mainApp.crs.mediaStorage.model import mediaStorage

class c_10_4_2(models.Model):
    name = models.CharField(default="Internet", max_length=250)
    subcriteria = models.ForeignKey(c_10_4, on_delete=models.CASCADE)
    description_file = models.FileField(upload_to="cr10/sub4/", max_length=200)
    media = models.ManyToManyField(mediaStorage)

    class Meta:
        app_label = "mainApp"