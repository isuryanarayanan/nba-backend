from django.db import models 
from mainApp.crs.criteriaTen.subCriteriaTwo.model import c_10_2
from mainApp.crs.mediaStorage.model import mediaStorage

class c_10_2_2(models.Model):
    name = models.CharField(default="Utilization of allocated funds", max_length=250)
    subcriteria = models.ForeignKey(c_10_2, on_delete=models.CASCADE)
    description_file = models.FileField(upload_to="cr10/sub2/", max_length=200)
    media = models.ManyToManyField(mediaStorage)

    class Meta:
        app_label = "mainApp"