from django.db import models 
from mainApp.crs.criteriaTen.model import c_10, c10addContent1
from mainApp.crs.proofStorage.model import proofStorage
from mainApp.crs.sampleStorage.model import sampleStorage
from mainApp.crs.mediaStorage.model import mediaStorage


class c10addContentCell(models.Model):
    col = models.IntegerField(default=0)
    row = models.IntegerField(default=0)
    content = models.CharField(default="", max_length=250)

    class Meta:
        app_label = "mainApp"

class c_10_2(models.Model):
    
    criteria = models.ForeignKey(c_10, on_delete=models.CASCADE) 

    name = models.CharField(default="Budget Allocation, Utilization, and Public Accounting at Institute level ", max_length=250)

    cfym0_2 = models.ManyToManyField(c10addContent1, related_name="cfym0_2")    
    cfym1_2 = models.ManyToManyField(c10addContent1, related_name="cfym1_2")
    cfym2_2 = models.ManyToManyField(c10addContent1, related_name="cfym2_2")
    cfym3_2 = models.ManyToManyField(c10addContent1, related_name="cfym3_2")
 
    table = models.ManyToManyField(c10addContentCell)

    description_file = models.FileField(upload_to="cr10/sub2/", max_length=200)
    proof = models.ManyToManyField(proofStorage)
    

    class Meta:
        app_label = "mainApp"