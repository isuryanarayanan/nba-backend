from django.db import models 

class c_7(models.Model):
    
    name = models.CharField(default="Continuous Improvement", max_length=250)

    class Meta:
        app_label = "mainApp"