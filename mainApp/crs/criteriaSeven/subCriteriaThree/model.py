from django.db import models 
from mainApp.crs.criteriaSeven.model import c_7
from mainApp.crs.mediaStorage.model import mediaStorage

class c_7_3(models.Model):

    criteria = models.ForeignKey(c_7, on_delete=models.CASCADE) 
    name = models.CharField(default="Improvement in Placement, Higher Studies and Entrepreneurship", max_length=250)

    item1 = models.CharField(default="Total No. of final year students", max_length=300)
    item2 = models.CharField(default="No. of students placed in companies or Government Sectors", max_length=300)
    item3 = models.CharField(default="No. of students admitted to higher studies with valid qualifying scores(GATE or equivalent State or National Level Tests,GRE,GMAT etc.", max_length=300)
    item4 = models.CharField(default="No. of students turned enterpreneur in engineering/technology", max_length=300)

    caym0_1 = models.CharField(default="", max_length=300)
    caym0_2 = models.CharField(default="", max_length=300)
    caym0_3 = models.CharField(default="", max_length=300)
    caym0_4 = models.CharField(default="", max_length=300)
    
    caym1_1 = models.CharField(default="", max_length=300)
    caym1_2 = models.CharField(default="", max_length=300)
    caym1_3 = models.CharField(default="", max_length=300)
    caym1_4 = models.CharField(default="", max_length=300)

    caym2_1 = models.CharField(default="", max_length=300)
    caym2_2 = models.CharField(default="", max_length=300)
    caym2_3 = models.CharField(default="", max_length=300)
    caym2_4 = models.CharField(default="", max_length=300)

    uploadFile_1 = models.FileField(upload_to="cr7/sub3/uploadFile/", max_length=200)
    uploadFile_2 = models.FileField(upload_to="cr7/sub3/uploadFile/", max_length=200)
    uploadFile_3 = models.FileField(upload_to="cr7/sub3/uploadFile/", max_length=200)
    uploadFile_4 = models.FileField(upload_to="cr7/sub3/uploadFile/", max_length=200)

    media = models.ManyToManyField(mediaStorage)

    class Meta:
        app_label = "mainApp"