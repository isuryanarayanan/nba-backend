from django.db import models 
from mainApp.crs.criteriaSeven.model import c_7
from mainApp.crs.sampleStorage.model import sampleStorage
class c_7_4(models.Model):

    name = models.CharField(default="7.4", max_length=250)
    criteria = models.ForeignKey(c_7, on_delete=models.CASCADE)
    #cay m0
    caym0_1_1 = models.CharField(default="No. of Students admitted", max_length=300)
    caym0_2_1 = models.CharField(default="Opening Score/Rank", max_length=300)
    caym0_3_1 = models.CharField(default="Closing Score/Rank", max_length=300)
    
    caym0_1_2 = models.CharField(default="No. of Students admitted", max_length=300)
    caym0_2_2 = models.CharField(default="Opening Score/Rank", max_length=300)
    caym0_3_2 = models.CharField(default="Closing Score/Rank", max_length=300)
    
    
    caym0_1_3 = models.CharField(default="No. of Students admitted", max_length=300)
    caym0_2_3 = models.CharField(default="Opening Score/Rank", max_length=300)
    caym0_3_3 = models.CharField(default="Closing Score/Rank", max_length=300)
    
    caym0_1_4 = models.CharField(default="Average CBSE/Any other Board Result of admitted students (Physics, Chemistry & Mathematics)", max_length=250)
    #cay m1
    caym1_1_1 = models.CharField(default="No. of Students admitted", max_length=300)
    caym1_2_1 = models.CharField(default="Opening Score/Rank", max_length=300)
    caym1_3_1 = models.CharField(default="Closing Score/Rank", max_length=300)
    
    caym1_1_2 = models.CharField(default="No. of Students admitted", max_length=300)
    caym1_2_2 = models.CharField(default="Opening Score/Rank", max_length=300)
    caym1_3_2 = models.CharField(default="Closing Score/Rank", max_length=300)
    
    caym1_1_3 = models.CharField(default="No. of Students admitted", max_length=300)
    caym1_2_3 = models.CharField(default="Opening Score/Rank", max_length=300)
    
    caym1_1_4 = models.CharField(default="Average CBSE/Any other Board Result of admitted students (Physics, Chemistry & Mathematics)", max_length=250)

    #cay m2
    caym2_1_1 = models.CharField(default="No. of Students admitted", max_length=300)
    caym2_2_1 = models.CharField(default="Opening Score/Rank", max_length=300)
    caym2_3_1 = models.CharField(default="Closing Score/Rank", max_length=300)
    
    caym2_1_2 = models.CharField(default="No. of Students admitted", max_length=300)
    caym2_2_2 = models.CharField(default="Opening Score/Rank", max_length=300)
    caym2_3_2 = models.CharField(default="Closing Score/Rank", max_length=300)
    
    caym2_1_3 = models.CharField(default="No. of Students admitted", max_length=300)
    caym2_2_3 = models.CharField(default="Opening Score/Rank", max_length=300)
    
    caym2_1_4 = models.CharField(default="Average CBSE/Any other Board Result of admitted students (Physics, Chemistry & Mathematics)", max_length=250)

    #proof
    proof_1 = models.FileField(upload_to="cr7/sub4/proof_1/", max_length=200)
    proof_2 = models.FileField(upload_to="cr7/sub4/proof_2/", max_length=200)
    proof_3 = models.FileField(upload_to="cr7/sub4/proof_3/", max_length=200)
    proof_4 = models.FileField(upload_to="cr7/sub4/proof_4/", max_length=200)

    sample = models.ManyToManyField(sampleStorage)
    class Meta:
        app_label = "mainApp"