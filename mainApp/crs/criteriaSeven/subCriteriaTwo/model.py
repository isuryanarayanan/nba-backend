from django.db import models 
from mainApp.crs.criteriaSeven.model import c_7
from mainApp.crs.proofStorage.model import proofStorage
from mainApp.crs.sampleStorage.model import sampleStorage

class c_7_2(models.Model):
    
    criteria = models.ForeignKey(c_7, on_delete=models.CASCADE) 

    name = models.CharField(default="Academic Audit and actions taken thereof during the period of Assessment", max_length=250)
    description_file = models.FileField(upload_to="cr7/sub2/", max_length=200)
    proof = models.ManyToManyField(proofStorage)
    sample = models.ManyToManyField(sampleStorage)

    class Meta:
        app_label = "mainApp"