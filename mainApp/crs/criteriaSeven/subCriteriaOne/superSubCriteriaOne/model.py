from django.db import models 
from mainApp.crs.criteriaSeven.subCriteriaOne.model import c_7_1, obv
from mainApp.crs.proofStorage.model import proofStorage
from mainApp.crs.sampleStorage.model import sampleStorage

class caym0(models.Model):

    pos = models.CharField(default="pos", max_length=250)
    targetLevel = models.CharField(default="target level", max_length=250)
    attainmentLevel = models.CharField(default="Attainment Level", max_length=250)
    observations = models.ManyToManyField(obv)
    actionPlan = models.TextField()

    class Meta:
        app_label = "mainApp"

class c_7_1_1(models.Model):
    
    subcriteria = models.ForeignKey(c_7_1, on_delete=models.CASCADE)
    proof = models.ManyToManyField(proofStorage)
    sample = models.ManyToManyField(sampleStorage)
    cay = models.ManyToManyField(caym0)

    class Meta:
        app_label = "mainApp"