from django.db import models 
from mainApp.crs.criteriaSeven.model import c_7
class obv(models.Model):

    description_file = models.FileField(upload_to="cr7/", max_length=200)

    class Meta:
        app_label = "mainApp"
class c_7_1(models.Model):
    criteria = models.ForeignKey(c_7, on_delete=models.CASCADE) 
    name = models.CharField(default="7.1", max_length=250)

    class Meta:
        app_label = "mainApp"