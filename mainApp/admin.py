from django.contrib import admin
from rcrud.bind import Publish
from rcrud.criteriaView import CRUDView
from mainApp.urls import urlpatterns as urls

from . crs.mediaStorage.model import mediaStorage
from . crs.proofStorage.model import proofStorage
from . crs.sampleStorage.model import sampleStorage

admin.site.register(mediaStorage)
urls += Publish(mediaStorage, 'media-storage/', CRUDView, _nots=True)
admin.site.register(proofStorage)
urls += Publish( proofStorage, 'proof-storage/', CRUDView, _nots=True)
admin.site.register(sampleStorage)
urls += Publish( sampleStorage, 'sample-storage/', CRUDView, _nots=True)
#criteria 1
from . crs.criteriaOne.model import c_1

#sub criterias 
from . crs.criteriaOne.subCriteriaOne.model import c_1_1
from . crs.criteriaOne.subCriteriaTwo.model import c_1_2
from . crs.criteriaOne.subCriteriaThree.model import c_1_3
from . crs.criteriaOne.subCriteriaFour.model import c_1_4
from . crs.criteriaOne.subCriteriaFive.model import c_1_5
from . crs.criteriaOne.subCriteriaFive.model import cell

admin.site.register(c_1)
urls += Publish(c_1, 'c_1/', CRUDView, _nots=True)
admin.site.register(c_1_1)
urls += Publish( c_1_1, 'c_1_1/', CRUDView, _nots=True)
admin.site.register(c_1_2)
urls += Publish( c_1_2, 'c_1_2/', CRUDView, _nots=True)
admin.site.register(c_1_3)
urls += Publish( c_1_3, 'c_1_3/', CRUDView, _nots=True)
admin.site.register(c_1_4)
urls += Publish( c_1_4, 'c_1_4/', CRUDView, _nots=True)
admin.site.register(c_1_5)
urls += Publish( c_1_5, 'c_1_5/', CRUDView, _nots=True)
admin.site.register(cell)
urls += Publish( cell, 'c_1_5/cell/', CRUDView, _nots=True)

#criteria 2
from . crs.criteriaTwo.model import c_2

#sub criterias
from . crs.criteriaTwo.subCriteriaOne.model import c_2_1
from . crs.criteriaTwo.subCriteriaTwo.model import c_2_2

#super sub criterias -one
from . crs.criteriaTwo.subCriteriaOne.superSubCriteriaOne.model import c_2_1_1
from . crs.criteriaTwo.subCriteriaOne.superSubCriteriaTwo.model import c_2_1_2
from . crs.criteriaTwo.subCriteriaOne.superSubCriteriaThree.model import c_2_1_3
from . crs.criteriaTwo.subCriteriaOne.superSubCriteriaFour.model import c_2_1_4

#super sub criterias -two
from . crs.criteriaTwo.subCriteriaTwo.superSubCriteriaOne.model import c_2_2_1
from . crs.criteriaTwo.subCriteriaTwo.superSubCriteriaTwo.model import c_2_2_2
from . crs.criteriaTwo.subCriteriaTwo.superSubCriteriaThree.model import c_2_2_3
from . crs.criteriaTwo.subCriteriaTwo.superSubCriteriaFour.model import c_2_2_4
from . crs.criteriaTwo.subCriteriaTwo.superSubCriteriaFive.model import c_2_2_5

admin.site.register(c_2)
urls += Publish( c_2, 'c_2/', CRUDView, _nots=True)
admin.site.register(c_2_1)
urls += Publish( c_2_1, 'c_2_1/', CRUDView, _nots=True)
admin.site.register(c_2_2)
urls += Publish( c_2_2, 'c_2_2/', CRUDView, _nots=True)

admin.site.register(c_2_1_1)
urls += Publish( c_2_1_1, 'c_2_1_1/', CRUDView, _nots=True)
admin.site.register(c_2_1_2)
urls += Publish( c_2_1_2, 'c_2_1_2/', CRUDView, _nots=True)
admin.site.register(c_2_1_3)
urls += Publish( c_2_1_3, 'c_2_1_3/', CRUDView, _nots=True)
admin.site.register(c_2_1_4)
urls += Publish( c_2_1_4, 'c_2_1_4/', CRUDView, _nots=True)

admin.site.register(c_2_2_1)
urls += Publish( c_2_2_1, 'c_2_2_1/', CRUDView, _nots=True)
admin.site.register(c_2_2_2)
urls += Publish( c_2_2_2, 'c_2_2_2/', CRUDView, _nots=True)
admin.site.register(c_2_2_3)
urls += Publish( c_2_2_3, 'c_2_2_3/', CRUDView, _nots=True)
admin.site.register(c_2_2_4)
urls += Publish( c_2_2_4, 'c_2_2_4/', CRUDView, _nots=True)
admin.site.register(c_2_2_5)
urls += Publish( c_2_2_5, 'c_2_2_5/', CRUDView, _nots=True)

#criteria 3
from . crs.criteriaThree.model import c_3
from . crs.criteriaThree.subCriteriaOne.model import c_3_1
from . crs.criteriaThree.subCriteriaOne.superSubCriteriaOne.model import c_3_1_1
from . crs.criteriaThree.subCriteriaOne.superSubCriteriaTwo.model import c_3_1_2
from . crs.criteriaThree.subCriteriaOne.superSubCriteriaThree.model import c_3_1_3

from . crs.criteriaThree.subCriteriaTwo.model import c_3_2
from . crs.criteriaThree.subCriteriaTwo.superSubCriteriaOne.model import c_3_2_1
from . crs.criteriaThree.subCriteriaTwo.superSubCriteriaTwo.model import c_3_2_2

from . crs.criteriaThree.subCriteriaThree.model import c_3_3
from . crs.criteriaThree.subCriteriaThree.superSubCriteriaOne.model import c_3_3_1
from . crs.criteriaThree.subCriteriaThree.superSubCriteriaTwo.model import c_3_3_2


admin.site.register(c_3)
urls += Publish( c_3, 'c_3/', CRUDView, _nots=True)

admin.site.register(c_3_1)
urls += Publish( c_3_1, 'c_3_1/', CRUDView, _nots=True)
admin.site.register(c_3_1_1)
urls += Publish( c_3_1_1, 'c_3_1_1/', CRUDView, _nots=True)
admin.site.register(c_3_1_2)
urls += Publish( c_3_1_2, 'c_3_1_2/', CRUDView, _nots=True)
admin.site.register(c_3_1_3)
urls += Publish( c_3_1_3, 'c_3_1_3/', CRUDView, _nots=True)

admin.site.register(c_3_2)
urls += Publish( c_3_2, 'c_3_2/', CRUDView, _nots=True)
admin.site.register(c_3_2_1)
urls += Publish( c_3_2_1, 'c_3_2_1/', CRUDView, _nots=True)
admin.site.register(c_3_2_2)
urls += Publish( c_3_2_2, 'c_3_2_2/', CRUDView, _nots=True)

admin.site.register(c_3_3)
urls += Publish( c_3_3, 'c_3_3/', CRUDView, _nots=True)
admin.site.register(c_3_3_1)
urls += Publish( c_3_3_1, 'c_3_3_1/', CRUDView, _nots=True)
admin.site.register(c_3_3_2)
urls += Publish( c_3_3_2, 'c_3_3_2/', CRUDView, _nots=True)

#criteria 5
from . crs.criteriaFive.model import c_5
from . crs.criteriaFive.subCriteriaOne.model import c_5_1
from . crs.criteriaFive.subCriteriaTwo.model import c_5_2
from . crs.criteriaFive.subCriteriaThree.model import c_5_3
from . crs.criteriaFive.subCriteriaFour.model import c_5_4
from . crs.criteriaFive.subCriteriaFive.model import c_5_5
from . crs.criteriaFive.subCriteriaSix.model import c_5_6, c5addContent

from . crs.criteriaFive.subCriteriaSeven.model import c_5_7,c_5_7addContent
from . crs.criteriaFive.subCriteriaSeven.superSubCriteriaOne.model import c_5_7_1
from . crs.criteriaFive.subCriteriaSeven.superSubCriteriaTwo.model import c_5_7_2
from . crs.criteriaFive.subCriteriaSeven.superSubCriteriaThree.model import c_5_7_3

from . crs.criteriaFive.subCriteriaEight.model import c_5_8
from . crs.criteriaFive.subCriteriaNine.model import c_5_9


admin.site.register(c_5)
urls += Publish( c_5, 'c_5/', CRUDView, _nots=True)
admin.site.register(c_5_1)
urls += Publish( c_5_1, 'c_5_1/', CRUDView, _nots=True)
admin.site.register(c_5_2)
urls += Publish( c_5_2, 'c_5_2/', CRUDView, _nots=True)
admin.site.register(c_5_3)
urls += Publish( c_5_3, 'c_5_3/', CRUDView, _nots=True)
admin.site.register(c_5_4)
urls += Publish( c_5_4, 'c_5_4/', CRUDView, _nots=True)
admin.site.register(c_5_5)
urls += Publish( c_5_5, 'c_5_5/', CRUDView, _nots=True)
admin.site.register(c_5_6)
urls += Publish( c_5_6, 'c_5_6/', CRUDView, _nots=True)
admin.site.register(c5addContent)
urls += Publish( c5addContent, 'c_5_6/add-content/', CRUDView, _nots=True)

admin.site.register(c_5_7)
urls += Publish( c_5_7, 'c_5_7/', CRUDView, _nots=True)
admin.site.register(c_5_7addContent)
urls += Publish( c_5_7addContent, 'c_5_7/add-content/', CRUDView, _nots=True)

admin.site.register(c_5_8)
urls += Publish( c_5_8, 'c_5_8/', CRUDView, _nots=True)
admin.site.register(c_5_9)
urls += Publish( c_5_9, 'c_5_9/', CRUDView, _nots=True)

#criteria 6
from . crs.criteriaSix.model import c_6

#sub criterias 
from . crs.criteriaSix.subCriteriaOne.model import c_6_1, addContent1
from . crs.criteriaSix.subCriteriaTwo.model import c_6_2, addContent2
from . crs.criteriaSix.subCriteriaThree.model import c_6_3
from . crs.criteriaSix.subCriteriaFour.model import c_6_4
from . crs.criteriaSix.subCriteriaFive.model import c_6_5

admin.site.register(c_6)
urls += Publish( c_6, 'c_6/', CRUDView, _nots=True)
admin.site.register(c_6_1)
urls += Publish( c_6_1, 'c_6_1/', CRUDView, _nots=True)
admin.site.register(addContent1)
urls += Publish( addContent1, 'c_6_1/add-content/', CRUDView, _nots=True)
admin.site.register(c_6_2)
urls += Publish( c_6_2, 'c_6_2/', CRUDView, _nots=True)
admin.site.register(addContent2)
urls += Publish( addContent2, 'c_6_2/add-content/', CRUDView, _nots=True)
admin.site.register(c_6_3)
urls += Publish( c_6_3, 'c_6_3/', CRUDView, _nots=True)
admin.site.register(c_6_4)
urls += Publish( c_6_4, 'c_6_4/', CRUDView, _nots=True)
admin.site.register(c_6_5)
urls += Publish( c_6_5, 'c_6_5/', CRUDView, _nots=True)




#criteria 7
from . crs.criteriaSeven.model import c_7

#sub criterias 
from . crs.criteriaSeven.subCriteriaOne.model import c_7_1, obv
from . crs.criteriaSeven.subCriteriaOne.superSubCriteriaOne.model import c_7_1_1
from . crs.criteriaSeven.subCriteriaOne.superSubCriteriaTwo.model import c_7_1_2
from . crs.criteriaSeven.subCriteriaOne.superSubCriteriaThree.model import c_7_1_3

from . crs.criteriaSeven.subCriteriaTwo.model import c_7_2
from . crs.criteriaSeven.subCriteriaThree.model import c_7_3
from . crs.criteriaSeven.subCriteriaFour.model import c_7_4


admin.site.register(c_7)
urls += Publish( c_7, 'c_7/', CRUDView, _nots=True)
admin.site.register(obv)
urls += Publish( c_7, 'c_7/obv/', CRUDView, _nots=True)
admin.site.register(c_7_1)
urls += Publish( c_7_1, 'c_7_1/', CRUDView, _nots=True)
admin.site.register(c_7_1_1)
urls += Publish( c_7_1_1, 'c_7_1_1/', CRUDView, _nots=True)
admin.site.register(c_7_1_2)
urls += Publish( c_7_1_2, 'c_7_1_2/', CRUDView, _nots=True)
admin.site.register(c_7_1_3)
urls += Publish( c_7_1_3, 'c_7_1_3/', CRUDView, _nots=True)

admin.site.register(c_7_2)
urls += Publish( c_7_2, 'c_7_2/', CRUDView, _nots=True)
admin.site.register(c_7_3)
urls += Publish( c_7_3, 'c_7_3/', CRUDView, _nots=True)
admin.site.register(c_7_4)
urls += Publish( c_7_4, 'c_7_4/', CRUDView, _nots=True)

#criteria 8
from . crs.criteriaEight.model import c_8
from . crs.criteriaEight.subCriteriaOne.model import c_8_1
from . crs.criteriaEight.subCriteriaTwo.model import c_8_2
from . crs.criteriaEight.subCriteriaThree.model import c_8_3
from . crs.criteriaEight.subCriteriaFour.model import c_8_4

from . crs.criteriaEight.subCriteriaFour.superSubCriteriaOne.model import c_8_4_1
from . crs.criteriaEight.subCriteriaFour.superSubCriteriaTwo.model import c_8_4_2

from . crs.criteriaEight.subCriteriaFive.model import c_8_5

from . crs.criteriaEight.subCriteriaFive.superSubCriteriaOne.model import c_8_5_1
from . crs.criteriaEight.subCriteriaFive.superSubCriteriaTwo.model import c_8_5_2

admin.site.register(c_8)
urls += Publish( c_8, 'c_8/', CRUDView, _nots=True)
admin.site.register(c_8_1)
urls += Publish( c_8_1, 'c_8_1/', CRUDView, _nots=True)
admin.site.register(c_8_2)
urls += Publish( c_8_2, 'c_8_2/', CRUDView, _nots=True)
admin.site.register(c_8_3)
urls += Publish( c_8_3, 'c_8_3/', CRUDView, _nots=True)
admin.site.register(c_8_4)
urls += Publish( c_8_4, 'c_8_4/', CRUDView, _nots=True)

admin.site.register(c_8_4_1)
urls += Publish( c_8_4_1, 'c_8_4_1/', CRUDView, _nots=True)
admin.site.register(c_8_4_2)
urls += Publish( c_8_4_2, 'c_8_4_2/', CRUDView, _nots=True)

admin.site.register(c_8_5)
urls += Publish( c_8_5, 'c_8_5/', CRUDView, _nots=True)

admin.site.register(c_8_5_1)
urls += Publish( c_8_5_1, 'c_8_5_1/', CRUDView, _nots=True)
admin.site.register(c_8_5_2)
urls += Publish( c_8_5_2, 'c_8_5_2/', CRUDView, _nots=True)

#criteria 9
from . crs.criteriaNine.model import c_9
from . crs.criteriaNine.subCriteriaOne.model import (
    c_9_1,
    appendixA,
    appendixB
)
from . crs.criteriaNine.subCriteriaTwo.model import (
    c_9_2,
    courseExitSurvey,
    teachingEffectiveness
)
from . crs.criteriaNine.subCriteriaThree.model import (
    c_9_3,
    programExitSurvey,
    feedbackFacilities,
    feedbackParents,
    addContent
)
from . crs.criteriaNine.subCriteriaFour.model import c_9_4
from . crs.criteriaNine.subCriteriaFive.model import c_9_5
from . crs.criteriaNine.subCriteriaSix.model import c_9_6
from . crs.criteriaNine.subCriteriaSeven.model import c_9_7

admin.site.register(c_9)
urls += Publish( c_9, 'c_9/', CRUDView, _nots=True)

admin.site.register(c_9_1)
urls += Publish( c_9_1, 'c_9_1/', CRUDView, _nots=True)
admin.site.register(appendixA)
urls += Publish( appendixA, 'c_9_1/appendix-a/', CRUDView, _nots=True)
admin.site.register(appendixB)
urls += Publish( appendixB, 'c_9_1/appendix-a/', CRUDView, _nots=True)

admin.site.register(c_9_2)
urls += Publish( c_9_2, 'c_9_2/', CRUDView, _nots=True)
admin.site.register(courseExitSurvey)
urls += Publish( courseExitSurvey, 'c_9_2/course-exit-survey/', CRUDView, _nots=True)
admin.site.register(teachingEffectiveness)
urls += Publish( teachingEffectiveness, 'c_9_2/teaching-effectiveness/', CRUDView, _nots=True)

admin.site.register(c_9_3)
urls += Publish( c_9_3, 'c_9_3/', CRUDView, _nots=True)
admin.site.register(programExitSurvey)
urls += Publish( programExitSurvey, 'c_9_3/program-exit-survey/', CRUDView, _nots=True)
admin.site.register(feedbackParents)
urls += Publish( feedbackParents, 'c_9_3/feedback-parents/', CRUDView, _nots=True)
admin.site.register(feedbackFacilities)
urls += Publish( feedbackFacilities, 'c_9_3/feedback-facilities/', CRUDView, _nots=True)
admin.site.register(addContent)
urls += Publish( addContent, 'c_9_3/add-content/', CRUDView, _nots=True)

admin.site.register(c_9_4)
urls += Publish( c_9_4, 'c_9_4/', CRUDView, _nots=True)
admin.site.register(c_9_5)
urls += Publish( c_9_5, 'c_9_5/', CRUDView, _nots=True)
admin.site.register(c_9_6)
urls += Publish( c_9_6, 'c_9_6/', CRUDView, _nots=True)
admin.site.register(c_9_7)
urls += Publish( c_9_7, 'c_9_7/', CRUDView, _nots=True)

#criteria 10
from . crs.criteriaTen.model import c_10,c10addContent1

from . crs.criteriaTen.subCriteriaOne.model import c_10_1
from . crs.criteriaTen.subCriteriaOne.superSubCriteriaOne.model import c_10_1_1
from . crs.criteriaTen.subCriteriaOne.superSubCriteriaTwo.model import c_10_1_2
from . crs.criteriaTen.subCriteriaOne.superSubCriteriaThree.model import c_10_1_3
from . crs.criteriaTen.subCriteriaOne.superSubCriteriaFour.model import c_10_1_4
from . crs.criteriaTen.subCriteriaOne.superSubCriteriaFive.model import c_10_1_5
from . crs.criteriaTen.subCriteriaOne.superSubCriteriaSix.model import c_10_1_6


from . crs.criteriaTen.subCriteriaTwo.model import c_10_2, c10addContentCell
from . crs.criteriaTen.subCriteriaTwo.superSubCriteriaOne.model import c_10_2_1
from . crs.criteriaTen.subCriteriaTwo.superSubCriteriaTwo.model import c_10_2_2
from . crs.criteriaTen.subCriteriaTwo.superSubCriteriaThree.model import c_10_2_3

from . crs.criteriaTen.subCriteriaThree.model import c_10_3
from . crs.criteriaTen.subCriteriaThree.superSubCriteriaOne.model import c_10_3_1
from . crs.criteriaTen.subCriteriaThree.superSubCriteriaTwo.model import c_10_3_2

from . crs.criteriaTen.subCriteriaFour.model import c_10_4
from . crs.criteriaTen.subCriteriaFour.superSubCriteriaOne.model import c_10_4_1
from . crs.criteriaTen.subCriteriaFour.superSubCriteriaTwo.model import c_10_4_2

admin.site.register(c_10)
urls += Publish( c_10, 'c_10/', CRUDView, _nots=True)
admin.site.register(c_10_1)
urls += Publish( c_10_1, 'c_10_1/', CRUDView, _nots=True)
admin.site.register(c_10_1_1)
urls += Publish( c_10_1_1, 'c_10_1_1/', CRUDView, _nots=True)
admin.site.register(c_10_1_2)
urls += Publish( c_10_1_2, 'c_10_1_2/', CRUDView, _nots=True)
admin.site.register(c_10_1_3)
urls += Publish( c_10_1_3, 'c_10_1_3/', CRUDView, _nots=True)
admin.site.register(c_10_1_4)
urls += Publish( c_10_1_4, 'c_10_1_4/', CRUDView, _nots=True)
admin.site.register(c_10_1_5)
urls += Publish( c_10_1_5, 'c_10_1_5/', CRUDView, _nots=True)
admin.site.register(c_10_1_6)
urls += Publish( c_10_1_6, 'c_10_1_6/', CRUDView, _nots=True)
admin.site.register(c10addContent1)
urls += Publish( c10addContent1, 'c_10/add-content-1/', CRUDView, _nots=True)


admin.site.register(c_10_2)
urls += Publish( c_10_2, 'c_10_2/', CRUDView, _nots=True)
admin.site.register(c_10_2_1)
urls += Publish( c_10_2_1, 'c_10_2_1/', CRUDView, _nots=True)
admin.site.register(c_10_2_2)
urls += Publish( c_10_2_2, 'c_10_2_2/', CRUDView, _nots=True)
admin.site.register(c_10_2_3)
urls += Publish( c_10_2_3, 'c_10_2_3/', CRUDView, _nots=True)
admin.site.register(c10addContentCell)
urls += Publish( c10addContentCell, 'c_10/add-content-cell/', CRUDView, _nots=True)

admin.site.register(c_10_3)
urls += Publish( c_10_3, 'c_10_3/', CRUDView, _nots=True)
admin.site.register(c_10_3_1)
urls += Publish( c_10_3_1, 'c_10_3_1/', CRUDView, _nots=True)
admin.site.register(c_10_3_2)
urls += Publish( c_10_3_2, 'c_10_3_2/', CRUDView, _nots=True)

admin.site.register(c_10_4)
urls += Publish( c_10_4, 'c_10_4/', CRUDView, _nots=True)
admin.site.register(c_10_4_1)
urls += Publish( c_10_4_1, 'c_10_4_1/', CRUDView, _nots=True)
admin.site.register(c_10_4_2)
urls += Publish( c_10_4_2, 'c_10_4_2/', CRUDView, _nots=True)
