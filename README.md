# Documentation NBA
## `` mediaStorage ``
```
    http://nbabackend.pythonanywhere.com/api/media/
```
> Fields
```
    data - file data 
```
## `` proofStorage ``
```
    http://nbabackend.pythonanywhere.com/api/proof/
```
> Fields
```
    data - file data 
```
## `` sampleStorage ``
```
    http://nbabackend.pythonanywhere.com/api/sample/
```
> Fields
```
    data - file data 
```
## `` criteria 1 ``

```
    http://nbabackend.pythonanywhere.com/api/c_1/
```
> Fields
```
    name - default value 
```
## Sub Criteria 1
```
    http://nbabackend.pythonanywhere.com/api/c_1/c_1_1/
```
> Fields
```
    name - default value 
    criteria - intiger foreign key
    description - text field
    media - many to many field to Media model
```
## Sub Criteria 2
```
    http://nbabackend.pythonanywhere.com/api/c_1/c_1_2/
```
> Fields
```
    name - default value 
    criteria - intiger foreign key
    description - text field
    media - many to many field to Media model
```
## Sub Criteria 3
```
    http://nbabackend.pythonanywhere.com/api/c_1/c_1_3/
```
> Fields
```
    name - default value 
    criteria - intiger foreign key
    description - text field
    media - many to many field to Media model
```
## Sub Criteria 4
```
    http://nbabackend.pythonanywhere.com/api/c_1/c_1_4/
```
> Fields
```
    name - default value 
    criteria - intiger foreign key
    description - text field
    media - many to many field to Media model
```
## Sub Criteria 5
```
    http://nbabackend.pythonanywhere.com/api/c_1/c_1_5/
```
> Fields
```
    name - default value
    criteria - intiger foriegn key
    number_missions - intiger
    number_pso - intiger
    cells - additional model 
    media - many to many field to Media model
```
## cells additional model
```
    http://nbabackend.pythonanywhere.com/api/c_1/c_1_5/cells/
```
> Fields
```
    cellData - charfield
    mission - intiger
    pso - intiger
```

## `` criteria 2 ``

```
    http://nbabackend.pythonanywhere.com/api/c_2/
```
> Fields
```
    name - default value 
```

## Sub Criteria 1
```
    http://nbabackend.pythonanywhere.com/api/c_2/c_2_1/
```
> Fields
```
    name - default value 
    criteria - intiger foreign key
    sample - many to many field to sample model
    proof - many to many field to proof model
```
### Super sub criteria 1
```
    http://nbabackend.pythonanywhere.com/api/c_2/c_2_1/c_2_1_1/
```
> Fields
```
    subcriteria - foreign key sub criteria
    name - text field
    description_file - file field
    curricular_gaps - additional field
```
### Curriculum gaps model
```
    http://nbabackend.pythonanywhere.com/api/c_2/c_2_1/c_2_1_1/curricular-gaps/
```
> Fields
```
    stream - charfield
    subject_with_gap - charfield
    workshop_planned - charfield
    po - charfield
    pso - charfield
```
### Super sub criteria 2
```
    http://nbabackend.pythonanywhere.com/api/c_2/c_2_1/c_2_1_2/
```
> Fields
```
    subcriteria- foreign key
    name - charfield
    course_code1 - charfield
    course_code2 - charfield
    course_title1 - charfield
    course_title2 - charfield
    lecture1 - charfield
    lecture2 - charfield
    total_lecture - charfield
    tutorial1 - charfield
    tutorial2 - charfield
    total_tutorial - charfield
    practical1 - charfield
    practical2 - charfield
    total_practical - charfield
    hours1 - charfield
    hours2 - charfield
    total_hours - charfield
    credits1 - charfield
    credits2 - charfield
    credits3 - charfield
    year - additional model
```
### yearData model
```
    http://nbabackend.pythonanywhere.com/api/c_2/c_2_1/c_2_1_2/year/
```
> Fields
```
    year - charfield
    slno - charfield
    gap - charfield
    action_taken - charfield
    dmy - charfield
    resource_person - charfield
    percentage - charfield
    relevant_po - charfield
```
### Super sub criteria 3
```
    http://nbabackend.pythonanywhere.com/api/c_2/c_2_1/c_2_1_3/
```
> Fields
```
    subcriteria - foreign key
    name - charfield
    basic_sciences1 - charfield
    basic_sciences2 - charfield
    basic_sciences3 - charfield
    engineering_sciences1 - charfield
    engineering_sciences2 - charfield
    engineering_sciences3 - charfield
    hum_and_social_sciences1 - charfield
    hum_and_social_sciences2 - charfield
    hum_and_social_sciences3 - charfield
    program_core1 - charfield
    program_core2 - charfield
    program_core3 - charfield
    program_electives1 - charfield
    program_electives2 - charfield
    program_electives3 - charfield
    open_electives1 - charfield
    open_electives2 - charfield
    open_electives3 - charfield
    projects1 - charfield
    projects2 - charfield
    projects3 - charfield
    internships_seminars1 - charfield
    internships_seminars2 - charfield
    internships_seminars3 - charfield
    any_other1 - charfield
    any_other2 - charfield
    any_other3 - charfield
    total - charfield
```
### Super Sub Criteria 4
```
    http://nbabackend.pythonanywhere.com/api/c_2/c_2_1/c_2_1_4/
```
> Fields
```
    subcriteria - foreign key
    name - charfield
    description - textfield
```

## Sub Criteria 2
```
    http://nbabackend.pythonanywhere.com/api/c_2/c_2_2/
```
> Fields
```
    name - default value 
    criteria - intiger foreign key
    samples - many to many field to sample model
    proofs - many to many field to proof model
```
### Super Sub Criteria 1
```
    http://nbabackend.pythonanywhere.com/api/c_2/c_2_2/c_2_2_1/
```
> Fields
```
    subcriteria - foreign key
    name - charfield
    description_file - filefield
```
### Super Sub Criteria 2
```
    http://nbabackend.pythonanywhere.com/api/c_2/c_2_2/c_2_2_2/
```
> Fields
```
    subcriteria - foreign key
    name - charfield
    description_file - filefield
```
### Super Sub Criteria 3
```
    http://nbabackend.pythonanywhere.com/api/c_2/c_2_2/c_2_2_3/
```
> Fields
```
    subcriteria - foreign key
    name - charfield
    description_file - filefield
```
### Super Sub Criteria 4
```
    http://nbabackend.pythonanywhere.com/api/c_2/c_2_2/c_2_2_4/
```
> Fields
```
    subcriteria - foreign key
    name - charfield
    description_file - filefield
```


## `` criteria 3 ``

```
    http://nbabackend.pythonanywhere.com/api/c_3/
```
> Fields
```
    name - default value 
```

## Sub Criteria 1
```
    http://nbabackend.pythonanywhere.com/api/c_3/c_3_1/
```
> Fields
```
    name - default value 
    criteria - intiger foreign key
```
### Super sub criteria 1
```
    http://nbabackend.pythonanywhere.com/api/c_3/c_3_1/c_3_1_1/
```
> Fields
```
    subcriteria - foreign key sub criteria
    name - text field
    media - many to many to media model
```

### Super sub criteria 2
```
    http://nbabackend.pythonanywhere.com/api/c_3/c_3_1/c_3_1_2/
```
> Fields
```
    subcriteria- foreign key
    name - charfield
    poAttainment - many to many to media model
```

### Super sub criteria 3
```
    http://nbabackend.pythonanywhere.com/api/c_3/c_3_1/c_3_1_3/
```
> Fields
```
    subcriteria - foreign key
    name - charfield
    media - many to many to media model
```


## Sub Criteria 2
```
    http://nbabackend.pythonanywhere.com/api/c_3/c_3_2/
```
> Fields
```
    name - default value 
    criteria - intiger foreign key
```
### Super Sub Criteria 1
```
    http://nbabackend.pythonanywhere.com/api/c_3/c_3_2/c_3_2_1/
```
> Fields
```
    subcriteria - foreign key
    name - charfield
    description - filefield
    media - many to many to media model
```
### Super Sub Criteria 2
```
    http://nbabackend.pythonanywhere.com/api/c_3/c_3_2/c_3_2_2/
```
> Fields
```
    subcriteria - foreign key
    name - charfield
    description - filefield
    media - many to many to media model
```


## Sub Criteria 3
```
    http://nbabackend.pythonanywhere.com/api/c_3/c_3_3/
```
> Fields
```
    name - default value 
    criteria - intiger foreign key
```
### Super Sub Criteria 1
```
    http://nbabackend.pythonanywhere.com/api/c_3/c_3_3/c_3_3_1/
```
> Fields
```
    subcriteria - foreign key
    name - charfield
    description - filefield
    media - many to many to media model
```
### Super Sub Criteria 2
```
    http://nbabackend.pythonanywhere.com/api/c_3/c_3_3/c_3_3_2/
```
> Fields
```
    subcriteria - foreign key
    name - charfield
    media - many to many to media model
```



## `` criteria 4 ``

```
    http://nbabackend.pythonanywhere.com/api/c_4/
```
> Fields
```
    name - default value 
    item_1 - charfield
    item_2 - charfield
    item_3 - charfield
    item_4 - charfield
    item_5 - charfield
    caym0_1 - charfield
    caym0_2 - charfield
    caym0_3 - charfield
    caym0_4 - charfield
    caym0_5 - charfield
    caym1_1 - charfield
    caym1_2 - charfield
    caym1_3 - charfield
    caym1_4 - charfield
    caym1_5 - charfield
    caym2_1 - charfield
    caym2_2 - charfield
    caym2_3 - charfield
    caym2_4 - charfield
    caym2_5 - charfield
    table2a - many to many to cay cell model
    table2b - many to many to cay cell model
```
###  caycell model

```
    http://nbabackend.pythonanywhere.com/api/c_4/cay-cell/
```
`` choices for caycell cay field ``
```
    ('caym0','caym0'),
    ('caym1','caym1'),
    ('caym2','caym2'),
    ('caym3','caym3'),
    ('lygm0','lygm0'),
    ('lygm1','lygm1'),
    ('lygm2','lygm2'),
````

> Fields
```
    cay - char choice field(send one of the choices)
    pos - intiger field
    media - many to many to media model
```

## Sub Criteria 1
```
    http://nbabackend.pythonanywhere.com/api/c_4/c_4_1/
```
> Fields
```
    name - default value 
    criteria - intiger foreign key

    year_1 - charfield
    year_2 - charfield
    year_3 - charfield
    n_1 - charfield
    n_2 - charfield
    n_3 - charfield
    n1_1 - charfield
    n1_2 - charfield
    n1_3 - charfield
    er_1 - charfield
    er_2 - charfield
    er_3 - charfield
```


## Sub Criteria 2
```
    http://nbabackend.pythonanywhere.com/api/c_4/c_4_2/
```
> Fields
```
    name - default value 
    criteria - intiger foreign key
```

### Super Sub Criteria 1
```
    http://nbabackend.pythonanywhere.com/api/c_4/c_4_2/c_4_2_1/
```
> Fields
```
    name - charfield
    criteria - foreign key to sub criteria
    item_1 - charfield
    item_2 - charfield
    item_3 - charfield
    lygm0_1 - charfield
    lygm0_2 - charfield
    lygm0_3 - charfield
    lygm1_1 - charfield
    lygm1_2 - charfield
    lygm1_3 - charfield
    lygm2_1 - charfield
    lygm2_2 - charfield
    lygm2_3 - charfield
```
### Super Sub Criteria 2
```
    http://nbabackend.pythonanywhere.com/api/c_4/c_4_2/c_4_2_2/
```
> Fields
```
    subcriteria - foreign key
    name - charfield
    media - many to many to media model
```

## Sub Criteria 3
```
    http://nbabackend.pythonanywhere.com/api/c_4/c_4_3/
```
> Fields
```
    name - default value 
    subcriteria - intiger foreign key
    item_1 - chaarfield
    item_2 - chaarfield
    item_3 - chaarfield
    item_4 - chaarfield
    caym0_1 - chaarfield
    caym0_2 - chaarfield
    caym0_3 - chaarfield
    caym0_4 - chaarfield
    caym1_1 - chaarfield
    caym1_2 - chaarfield
    caym1_3 - chaarfield
    caym1_4 - chaarfield
    caym2_1 - chaarfield
    caym2_2 - chaarfield
    caym2_3 - chaarfield
    caym2_4 - chaarfield
```

## Sub Criteria 4
```
    http://nbabackend.pythonanywhere.com/api/c_4/c_4_4/
```
> Fields
```
    name - default value 
    subcriteria - intiger foreign key
    item_1 - chaarfield
    item_2 - chaarfield
    item_3 - chaarfield
    item_4 - chaarfield

    caym0_1 - chaarfield
    caym0_2 - chaarfield
    caym0_3 - chaarfield
    caym0_4 - chaarfield

    caym1_1 - chaarfield
    caym1_2 - chaarfield
    caym1_3 - chaarfield
    caym1_4 - chaarfield

    caym2_1 - chaarfield
    caym2_2 - chaarfield
    caym2_3 - chaarfield
    caym2_4 - chaarfield
```
## Sub Criteria 5
```
    http://nbabackend.pythonanywhere.com/api/c_4/c_4_5/
```
> Fields
```
    name - default value 
    subcriteria - intiger foreign key
    item_1 - charfield
    item_2 - charfield
    item_3 - charfield
    item_4 - charfield
    item_5 - charfield
    item_6 - charfield
    lygm0_1 - charfield
    lygm0_2 - charfield
    lygm0_3 - charfield
    lygm0_4 - charfield
    lygm0_5 - charfield
    lygm0_6 - charfield
    lygm1_1 - charfield
    lygm1_2 - charfield
    lygm1_3 - charfield
    lygm1_4 - charfield
    lygm1_5 - charfield
    lygm1_6 - charfield
    lygm2_1 - charfield
    lygm2_2 - charfield
    lygm2_3 - charfield
    lygm2_4 - charfield
    lygm2_5 - charfield
    lygm2_6 - charfield
```

## Sub Criteria 6
```
    http://nbabackend.pythonanywhere.com/api/c_4/c_4_6/
```
> Fields
```
    name - default value 
    subcriteria - intiger foreign key

```


### Super Sub Criteria 1
```
    http://nbabackend.pythonanywhere.com/api/c_4/c_4_6/c_4_6_1/
```
> Fields
```
    subcriteria - foreign key
    name - charfield
    description_file - filefield
    media - many to many to media model
```
### Super Sub Criteria 2
```
    http://nbabackend.pythonanywhere.com/api/c_4/c_4_6/c_4_6_2/
```
> Fields
```
    subcriteria - foreign key
    name - charfield
    media - many to many to media model
```

### Super Sub Criteria 3
```
    http://nbabackend.pythonanywhere.com/api/c_4/c_4_6/c_4_6_3/
```
> Fields
```
    subcriteria - foreign key
    name - charfield
    year_1_1 - charfield
    year_1_2 - charfield
    year_1_3 - charfield
    year_1_4 - charfield
    year_2_1 - charfield
    year_2_2 - charfield
    year_2_3 - charfield
    year_2_4 - charfield
    year_3_1 - charfield
    year_3_2 - charfield
    year_3_3 - charfield
    year_3_4 - charfield
    media - many to many to media model
```


## `` criteria 5 ``

```
    http://nbabackend.pythonanywhere.com/api/c_5/
```
> Fields
```
    name - default value 
    description - textfield
    media - many to many to media model
```

## Sub Criteria 1
```
    http://nbabackend.pythonanywhere.com/api/c_5/c_5_1/
```
> Fields
```
    name - default value 
    criteria - intiger foreign key
    caym0_1 - charfield
    caym1_1 - charfield
    caym2_1 - charfield
    caym0_2 - charfield
    caym1_2 - charfield
    caym2_2 - charfield
    caym0_3 - charfield
    caym1_3 - charfield
    caym2_3 - charfield
```


## Sub Criteria 2
```
    http://nbabackend.pythonanywhere.com/api/c_5/c_5_2/
```
> Fields
```
    name - default value 
    criteria - intiger foreign key
    caym0_1 - charfield
    caym1_1 - charfield
    caym2_1 - charfield
    caym0_2 - charfield
    caym1_2 - charfield
    caym2_2 - charfield
    caym0_3 - charfield
    caym1_3 - charfield
    caym2_3 - charfield
    caym0_4 - charfield
    caym1_4 - charfield
    caym2_4 - charfield
    caym0_5 - charfield
    caym1_5 - charfield
    caym2_5 - charfield
    caym0_6 - charfield
    caym1_6 - charfield
    caym2_6 - charfield
```

## Sub Criteria 3
```
    http://nbabackend.pythonanywhere.com/api/c_5/c_5_3/
```
> Fields
```
    name - default value 
    criteria - intiger foreign key
    caym0_1 - charfield
    caym1_1 - charfield
    caym2_1 - charfield
    caym0_2 - charfield
    caym1_2 - charfield
    caym2_2 - charfield
    caym0_3 - charfield
    caym1_3 - charfield
    caym2_3 - charfield
    caym0_4 - charfield
    caym1_4 - charfield
    caym2_4 - charfield
    average_assessment_4 - charfield
```

## Sub Criteria 4
```
    http://nbabackend.pythonanywhere.com/api/c_5/c_5_4/
```
> Fields
```
    name - default value 
    criteria - intiger foreign key


    caym0_1 - charfield
    caym1_1 - charfield
    caym2_1 - charfield
    caym0_2 - charfield
    caym1_2 - charfield
    caym2_2 - charfield
    caym0_3 - charfield
    caym1_3 - charfield
    caym2_3 - charfield
```