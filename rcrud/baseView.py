from rest_framework.serializers import ModelSerializer
from django.views.decorators.csrf import csrf_exempt
from rest_framework.decorators import api_view
from rest_framework.response import Response
from django.http import JsonResponse
from django.contrib import admin


@api_view(['GET', ])
def _detailGet(request, id, _model):

    class _serializer(ModelSerializer):
        class Meta:
            model = _model
            fields = '__all__'

    try:
        objects = _model.objects.get(id=id)
    except _model.DoesNotExist:
        return Response({'object':'not found'},status=404)

    if request.method == 'GET':
        serializer = _serializer(objects)
        return Response(serializer.data)

@api_view(['PUT', ])
def _put(request, id, _model):

    class _serializer(ModelSerializer):
        class Meta:
            model = _model
            fields = '__all__'

    try:
        objects = _model.objects.get(id=id)
    except _model.DoesNotExist:
        return Response({'object':'not found'},status=404)

    if request.method == 'PUT':
        serializer = _serializer(objects,data=request.data, partial=True)
    if serializer.is_valid():
        serializer.save()
        return Response(serializer.data)
    else:
        return Response(serializer.errors)

@api_view(['DELETE', ])
def _delete(request, id, _model):

    try:
        objects = _model.objects.get(id=id)
    except _model.DoesNotExist:
        return Response({'object':'not found'},status=404)

    if request.method == 'DELETE':
        if(objects):
            objects.delete()
            return Response({'success':'instance deleted'}, status = 200)
        else:
            return Response('Not Found', status = 404)
    else:
        return Response('Not Found', status = 404)


@api_view(['GET', ])
def _get(request, _model):

    class _serializer(ModelSerializer):
        class Meta:
            model = _model
            fields = '__all__'

    if request.method == 'GET':
        serializer = _serializer(_model.objects.all(), many=True)
        return Response(serializer.data)
        
@api_view(['POST', ])
def _post(request, _model):

    class _serializer(ModelSerializer):
        class Meta:
            model = _model
            fields = '__all__'

    if request.method == 'POST':
        serializer = _serializer(data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data)
        else:
            return Response(serializer.errors)



@csrf_exempt
def CRUDView(request,id=None,**kwargs):
    _model = kwargs['model']
    processed = False
    if id ==  None:
        if request.method == 'GET':
            processed = True
            return _get(request, _model)
        if request.method == 'POST':
            processed = True
            return _post(request, _model)
    else:
        if request.method == 'GET':
            processed = True
            return _detailGet(request, id, _model)
        if request.method == 'PUT':
            processed = True
            return _put(request, id, _model)
        if request.method == 'DELETE':
            processed = True
            return _delete(request, id, _model)

    if(processed==False):
        return JsonResponse({'request':'unsupported type'})