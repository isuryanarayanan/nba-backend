
"""
(made to serve odtasker project)
This app is to just simplify the process of
creating a CRUD api for a specific model by
reusing the redundant pieces of code 

How to use
==========

step 1: 
    
    Create a model you want to bind with
    a crud api and migrate to database

step 2: 
    
    Call the bind.Publish method with a
    bunch of parameter here and append it to 
    url patterns from the urls file of the app
    under which youre binding and you're
    done.

Syntax
======

Importing bind methods
`from rcrud.bind import Publish`

simulating a urls.py file! but you can bind from 
anywhere but should be with a url patternsas the 
publish method returns a django urls path

`
urlpatterns = [
    "some other paths if any"
]

urlpatterns += Publish(
   Youre Model,
   Endpoint path,
   _build = BOOLEAN, # _build true to generate views   
   _logic = Your own views , 
)
`
The publsih method will return 2 paths 
a list view with the given Endpoint path/ and
a detail view with the Endpoint path/id/

There you go no more api folders for rest crud :)
"""

# Django generic imports
from django.views.decorators.csrf import csrf_exempt
from django.http    import JsonResponse
from django.contrib import admin
from django.urls    import path

# Rest framework
from rest_framework.serializers import ModelSerializer
from rest_framework.decorators  import api_view
from rest_framework.response    import Response

# System import
from os.path    import isfile, join
from os         import listdir
import importlib
import shutil
import os

# Detail GET request handler
@api_view(['GET', ])
def _detailGet(request, id, _model):

    class _serializer(ModelSerializer):
        class Meta:
            model = _model
            fields = '__all__'

    try:
        objects = _model.objects.get(id=id)
    except _model.DoesNotExist:
        return Response({'object':'not found'},status=404)
    if request.method == 'GET':
        serializer = _serializer(objects)
        return Response(serializer.data)

# Detail PUT request handler
@api_view(['PUT', ])
def _put(request, id, _model):
    class _serializer(ModelSerializer):
        class Meta:
            model = _model
            fields = '__all__'
    try:
        objects = _model.objects.get(id=id)
    except _model.DoesNotExist:
        return Response({'object':'not found'},status=404)
    if request.method == 'PUT':
        serializer = _serializer(objects,data=request.data, partial=True)
    if serializer.is_valid():
        serializer.save()
        return Response(serializer.data)
    else:
        return Response(serializer.errors)

# Detail DELETE request handler
@api_view(['DELETE', ])
def _delete(request, id, _model):
    try:
        objects = _model.objects.get(id=id)
    except _model.DoesNotExist:
        return Response({'object':'not found'},status=404)
    if request.method == 'DELETE':
        if(objects):
            objects.delete()
            return Response({'success':'instance deleted'}, status = 200)
        else:
            return Response('Not Found', status = 404)
    else:
        return Response('Not Found', status = 404)

# List GET request handler
@api_view(['GET', ])
def _get(request, _model):
    class _serializer(ModelSerializer):
        class Meta:
            model = _model
            fields = '__all__'
    if request.method == 'GET':
        serializer = _serializer(_model.objects.all(), many=True)
        return Response(serializer.data)
        
# List POST request handler
@api_view(['POST', ])
def _post(request, _model):
    class _serializer(ModelSerializer):
        class Meta:
            model = _model
            fields = '__all__'

    if request.method == 'POST':
        serializer = _serializer(data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data)
        else:
            return Response(serializer.errors)


# Request handler
# Directs request based on the request method
@csrf_exempt
def CRUDView(request,id=None,**kwargs):
    _model = kwargs['model']
    processed = False
    if id ==  None:
        if request.method == 'GET':
            processed = True
            return _get(request, _model)
        if request.method == 'POST':
            processed = True
            return _post(request, _model)
    else:
        if request.method == 'GET':
            processed = True
            return _detailGet(request, id, _model)
        if request.method == 'PUT':
            processed = True
            return _put(request, id, _model)
        if request.method == 'DELETE':
            processed = True
            return _delete(request, id, _model)

    if(processed==False):
        return JsonResponse({'request':'unsupported type'})

# Build views file for _build command
def buildLogicTreeFor(_model):
    _buildName = str(_model._meta.object_name)
    _folderName = 'logic'
    _buildName = str(_model._meta.object_name)+"Logic.py"
    from django.conf import settings
    bd = settings.BASE_DIR
    src = str(bd)+'/rcrud/baseView.py'
    dst = str(bd)+'/'+_model._meta.app_label+'/'+_folderName+'/'+_model._meta.object_name+'/'
    if not os.path.exists(dst):
        os.makedirs(dst)
    allFiles = [f for f in listdir(dst) if isfile(join(dst, f))]

    if _buildName in allFiles:
        pass
    else:
        shutil.copy(src, dst)
        os.rename(dst+'baseView.py', dst+str(_model._meta.object_name)+'Logic.py')
    
    dst = ''+_model._meta.app_label+'.'+_folderName+'.'+_model._meta.object_name+'.'+str(_model._meta.object_name)+'Logic'
    return(dst)

# Base bind method 
# binds the parameters
def Publish(_model, _path, _logic=None, _build=False, _nots=False):

    # Checking build
    if _build == True:
        logicPath = buildLogicTreeFor(_model)
        try:
            _logic = importlib.import_module(logicPath)
        except ImportError:
            print('Import Error')
    
    # Checking logic
    if _logic != None and _build == True:
        return [
            path(

                    _path,
                    _logic.CRUDView,
                    kwargs={
                        'model': _model,
                        'nots': _nots
                    }

            ),
            path(

                    _path+'<int:id>/',
                    _logic.CRUDView,
                    kwargs={
                        'model': _model,
                        'nots': _nots
                    }

            )
        ]
    elif _logic != None and _build == False:
        return [
            path(

                    _path,
                    _logic,
                    kwargs={
                        'model': _model,
                        'nots': _nots
                    }

            ),
            path(

                    _path+'<int:id>/',
                    _logic,
                    kwargs={
                        'model': _model,
                        'nots': _nots
                    }

            )
        ]
    else:
        return [
            path(

                    _path,
                    CRUDView,
                    kwargs={
                        'model': _model,
                        'nots': _nots
                    }

            ),
            path(

                    _path+'<int:id>/',
                    CRUDView,
                    kwargs={
                        'model': _model,
                        'nots': _nots
                    }

            )
        ]

